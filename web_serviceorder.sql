-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 05 Apr 2020 pada 09.53
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_serviceorder`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mcustomer`
--

CREATE TABLE `mcustomer` (
  `ID_Customer` bigint(10) NOT NULL,
  `NM_Customer` varchar(200) NOT NULL,
  `NM_Address` varchar(200) NOT NULL,
  `NM_PhoneNo` varchar(50) DEFAULT NULL,
  `NM_Email` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mcustomer`
--

INSERT INTO `mcustomer` (`ID_Customer`, `NM_Customer`, `NM_Address`, `NM_PhoneNo`, `NM_Email`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(2, 'GOMGOM', 'Medan', '123456', 'gomgom@parsoburancity.go.id', 'admin', '2020-01-28 18:48:20', NULL, NULL),
(3, 'JUARA MONANG', 'Medan', '123456', 'juara.monang@gmail.com', 'admin', '2020-01-31 12:05:41', NULL, NULL),
(8, 'Partopi Tao', 'Tao Toba Nauli Regency, North Sumatera, Indonesia', '085359867032', 'rolassimanjuntak@gmail.com', 'rolassimanjuntak@gmail.com', '2020-02-05 15:30:30', NULL, NULL),
(9, 'Atin Simanjuntak', '-', '123456', 'atinjuntak@gmail.com', 'atinjuntak@gmail.com', '2020-03-07 10:25:02', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mmechanic`
--

CREATE TABLE `mmechanic` (
  `ID_Mechanic` bigint(10) NOT NULL,
  `NM_Mechanic` varchar(50) NOT NULL,
  `TH_Bergabung` int(11) NOT NULL,
  `NO_Telp` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mmechanic`
--

INSERT INTO `mmechanic` (`ID_Mechanic`, `NM_Mechanic`, `TH_Bergabung`, `NO_Telp`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 'JORBUT', 2020, '123456', 'admin', '2020-01-28 18:19:25', 'admin', '2020-01-28 18:22:48'),
(2, 'MARULAK', 2018, '123456', 'admin', '2020-01-28 18:22:05', NULL, NULL),
(4, 'LAMHOT', 2018, '123456', 'admin', '2020-01-28 18:23:08', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mservice`
--

CREATE TABLE `mservice` (
  `ID_Service` bigint(10) NOT NULL,
  `NM_Service` varchar(100) NOT NULL,
  `Duration` double NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mservice`
--

INSERT INTO `mservice` (`ID_Service`, `NM_Service`, `Duration`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 'Service Ringan', 45, 'admin', '2020-01-30 16:18:58', NULL, NULL),
(2, 'Service Injeksi', 60, 'admin', '2020-01-30 16:19:11', NULL, NULL),
(3, 'Overhaul', 60, 'admin', '2020-01-30 16:19:26', 'admin', '2020-01-30 16:19:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mstatus`
--

CREATE TABLE `mstatus` (
  `ID_Status` bigint(10) NOT NULL,
  `NM_Status` varchar(50) NOT NULL,
  `NM_LabelColor` varchar(10) NOT NULL,
  `IS_Project` tinyint(1) NOT NULL DEFAULT '1',
  `IS_Default` tinyint(1) DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mstatus`
--

INSERT INTO `mstatus` (`ID_Status`, `NM_Status`, `NM_LabelColor`, `IS_Project`, `IS_Default`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(9, 'Menunggu', '#CCCCCC', 1, 0, 'admin', '2020-01-27 16:23:25', 'admin', '2020-04-05 06:33:02'),
(10, 'Sedang Diproses', '#FFFF99', 1, 0, 'admin', '2020-01-27 16:24:18', 'admin', '2020-04-05 06:32:30'),
(11, 'Selesai', '#99FFCC', 1, 0, 'admin', '2020-01-27 16:25:21', 'admin', '2020-04-05 06:32:53'),
(12, 'Batal', '#FFCCCC', 1, 0, 'admin', '2020-01-27 16:25:45', 'admin', '2020-04-05 06:32:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mvehicle`
--

CREATE TABLE `mvehicle` (
  `ID_Vehicle` bigint(20) NOT NULL,
  `ID_Type` bigint(20) NOT NULL,
  `ID_Customer` bigint(20) NOT NULL,
  `NM_Pemilik` varchar(100) NOT NULL,
  `NO_Plat` varchar(100) DEFAULT NULL,
  `TH_Pembuatan` int(11) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mvehicle`
--

INSERT INTO `mvehicle` (`ID_Vehicle`, `ID_Type`, `ID_Customer`, `NM_Pemilik`, `NO_Plat`, `TH_Pembuatan`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 4, 2, 'Gomgom Parulak-ulak', 'BK 13055 AA', 2012, 'admin', '2020-01-28 18:48:20', NULL, NULL),
(4, 3, 2, 'Gomgom Parulak-ulak', 'BK 13057 AA', 2010, 'admin', '2020-01-29 04:02:53', NULL, NULL),
(5, 8, 3, 'Juara Monang', 'BK 1212 ABC', 2018, 'admin', '2020-01-31 12:05:41', NULL, NULL),
(9, 14, 8, 'Yoel Rolas Simanjuntak', 'BK 2924 AGD', 2018, 'rolassimanjuntak@gmail.com', '2020-02-05 15:30:30', NULL, NULL),
(10, 3, 9, '123456', 'BK 1212 AGD', 2018, 'atinjuntak@gmail.com', '2020-03-07 10:25:02', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mvehicletype`
--

CREATE TABLE `mvehicletype` (
  `ID_Type` bigint(10) NOT NULL,
  `NM_Type` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mvehicletype`
--

INSERT INTO `mvehicletype` (`ID_Type`, `NM_Type`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(3, 'Jupiter Z', 'admin', '2020-01-28 14:30:39', NULL, NULL),
(4, 'Jupiter MX', 'admin', '2020-01-28 14:30:48', NULL, NULL),
(5, 'Scorpio', 'admin', '2020-01-28 14:31:00', NULL, NULL),
(6, 'Mio Z', 'admin', '2020-01-28 14:32:01', NULL, NULL),
(7, 'Mio S', 'admin', '2020-01-28 14:32:08', NULL, NULL),
(8, 'Vixion', 'admin', '2020-01-28 14:33:06', NULL, NULL),
(9, 'Vixion R', 'admin', '2020-01-28 14:33:14', 'admin', '2020-01-28 14:37:55'),
(11, 'Aerox 155', 'admin', '2020-01-28 14:34:05', NULL, NULL),
(12, 'NMAX', 'admin', '2020-01-28 14:34:12', NULL, NULL),
(13, 'XMAX', 'admin', '2020-01-28 14:34:17', NULL, NULL),
(14, 'NMAX - ABS', 'admin', '2020-01-28 14:34:45', 'admin', '2020-01-28 14:35:04'),
(15, 'Lexi S', 'admin', '2020-01-28 14:34:52', NULL, NULL),
(16, 'Lexi', 'admin', '2020-01-28 14:35:11', NULL, NULL),
(17, 'Lexi S - ABS', 'admin', '2020-01-28 14:35:24', NULL, NULL),
(18, 'XSR 155', 'admin', '2020-01-28 14:38:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `postcategories`
--

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) UNSIGNED NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `postcategories`
--

INSERT INTO `postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`) VALUES
(1, 'Berita', '#f56954'),
(5, 'Others', '#3c8dbc');

-- --------------------------------------------------------

--
-- Struktur dari tabel `postimages`
--

CREATE TABLE `postimages` (
  `PostImageID` bigint(10) NOT NULL,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE `posts` (
  `PostID` bigint(20) UNSIGNED NOT NULL,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posts`
--

INSERT INTO `posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(4, 5, '2019-06-26', 'Hubungi Kami', 'hubungi-kami', '<p><strong>Dinas Komunikasi Dan Informatika Kabupaten Humbang Hasundutan</strong></p>\r\n\r\n<p>Jl. SM. Raja Kompleks Perkantoran Tano Tubu, Doloksanggul 22457</p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Telp</td>\r\n			<td>:</td>\r\n			<td>&nbsp;(0633) 31555</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email&nbsp;</td>\r\n			<td>:</td>\r\n			<td colspan=\"4\">&nbsp;diskominfo@humbanghasundutankab.go.id</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.6996793830294!2d98.76777421426368!3d2.265541658581495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e3cb7ff0bcc73%3A0x2c1a5b16f34531dc!2sDinas+Komunikasi+dan+Informatika+Kab.+Humbang+Hasundutan!5e0!3m2!1sen!2sid!4v1561433370666!5m2!1sen!2sid\" style=\"border:0\" width=\"600\"></iframe></p>\r\n', '2020-12-31', 9, NULL, 0, NULL, 'admin', '2019-06-26 08:27:46', 'admin', '2019-06-26 08:27:46'),
(5, 1, '2020-04-05', 'Selamat Datang', 'selamat-datang', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '2021-04-05', 13, NULL, 0, NULL, 'admin', '2020-04-05 08:34:38', 'admin', '2020-04-05 09:08:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `RoleID` int(10) UNSIGNED NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`RoleID`, `RoleName`) VALUES
(1, 'Administrator'),
(2, 'Customer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `SettingID` int(10) UNSIGNED NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`) VALUES
(1, 'SETTING_WEB_NAME', 'SETTING_WEB_NAME', 'Service Order System'),
(2, 'SETTING_WEB_DESC', 'SETTING_WEB_DESC', 'Sistem Informasi Antrian Servis'),
(3, 'SETTING_WEB_DISQUS_URL', 'SETTING_WEB_DISQUS_URL', 'https://general-9.disqus.com/embed.js'),
(4, 'SETTING_ORG_NAME', 'SETTING_ORG_NAME', 'PT. ALFA SCORPII'),
(5, 'SETTING_ORG_ADDRESS', 'SETTING_ORG_ADDRESS', 'PT Alfa Scorpii Setia Budi, Jl. Setia Budi No.74 DEF Medan'),
(6, 'SETTING_ORG_LAT', 'SETTING_ORG_LAT', ''),
(7, 'SETTING_ORG_LONG', 'SETTING_ORG_LONG', ''),
(8, 'SETTING_ORG_PHONE', 'SETTING_ORG_PHONE', '(061) 8218955'),
(9, 'SETTING_ORG_FAX', 'SETTING_ORG_FAX', '(061) 8218708'),
(10, 'SETTING_ORG_MAIL', 'SETTING_ORG_MAIL', '-'),
(11, 'SETTING_WEB_API_FOOTERLINK', 'SETTING_WEB_API_FOOTERLINK', '-'),
(12, 'SETTING_WEB_LOGO', 'SETTING_WEB_LOGO', 'logo.png'),
(13, 'SETTING_WEB_SKIN_CLASS', 'SETTING_WEB_SKIN_CLASS', 'skin-green-light'),
(14, 'SETTING_WEB_PRELOADER', 'SETTING_WEB_PRELOADER', 'Pulse-1s-200px.gif'),
(15, 'SETTING_WEB_VERSION', 'SETTING_WEB_VERSION', '1.0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `torder`
--

CREATE TABLE `torder` (
  `ID_Order` bigint(10) NOT NULL,
  `ID_Customer` bigint(10) NOT NULL,
  `ID_Service` bigint(10) NOT NULL,
  `ID_Vehicle` bigint(10) NOT NULL,
  `ID_Mechanic` bigint(10) DEFAULT NULL,
  `ID_Status` bigint(10) NOT NULL,
  `Date` date NOT NULL,
  `NO_Telp` varchar(50) DEFAULT NULL,
  `RE_Customer` varchar(200) DEFAULT NULL,
  `RE_Mechanic` varchar(200) DEFAULT NULL,
  `Remarks` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `torder`
--

INSERT INTO `torder` (`ID_Order`, `ID_Customer`, `ID_Service`, `ID_Vehicle`, `ID_Mechanic`, `ID_Status`, `Date`, `NO_Telp`, `RE_Customer`, `RE_Mechanic`, `Remarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(11, 8, 2, 9, 1, 11, '2020-03-07', '123456', 'TEST', 'Ganti oli', '-', 'rolassimanjuntak@gmail.com', '2020-03-07 10:27:08', 'admin', '2020-03-07 10:29:25'),
(12, 9, 1, 10, 4, 11, '2020-03-07', '123456', 'Test', NULL, NULL, 'atinjuntak@gmail.com', '2020-03-07 10:30:43', NULL, NULL),
(13, 8, 2, 9, 1, 11, '2020-04-05', '085359867032', 'TEST', NULL, NULL, 'rolassimanjuntak@gmail.com', '2020-04-05 07:30:44', NULL, NULL),
(15, 8, 1, 9, 1, 9, '2020-04-05', '085359867032', 'TEST', NULL, NULL, 'rolassimanjuntak@gmail.com', '2020-04-05 08:37:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `torder_status`
--

CREATE TABLE `torder_status` (
  `Uniq` bigint(10) NOT NULL,
  `ID_Order` bigint(10) NOT NULL,
  `ID_Status` bigint(10) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `torder_status`
--

INSERT INTO `torder_status` (`Uniq`, `ID_Order`, `ID_Status`, `Timestamp`) VALUES
(10, 11, 9, '2020-03-07 09:27:08'),
(11, 11, 10, '2020-03-07 09:29:25'),
(12, 12, 9, '2020-03-07 09:30:43'),
(13, 12, 11, '2020-04-05 05:05:08'),
(14, 11, 11, '2020-04-05 05:05:12'),
(15, 13, 9, '2020-04-05 05:30:44'),
(16, 13, 11, '2020-04-05 06:33:35'),
(17, 15, 9, '2020-04-05 06:37:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `userinformation`
--

CREATE TABLE `userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `userinformation`
--

INSERT INTO `userinformation` (`UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`) VALUES
('admin', 'yoelrolas@gmail.com', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-17'),
('atinjuntak@gmail.com', 'atinjuntak@gmail.com', '9', 'Atin Simanjuntak', NULL, NULL, NULL, NULL, '-', '123456', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07'),
('rolassimanjuntak@gmail.com', 'rolassimanjuntak@gmail.com', '8', 'Partopi Tao', NULL, NULL, NULL, NULL, 'Tao Toba Nauli Regency, North Sumatera, Indonesia', '085359867032', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) UNSIGNED NOT NULL,
  `IsSuspend` tinyint(1) UNSIGNED NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`) VALUES
('admin', 'e10adc3949ba59abbe56e057f20f883e', 1, 0, '2020-04-05 05:31:20', '::1'),
('atinjuntak@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 0, '2020-03-07 10:25:15', '::1'),
('rolassimanjuntak@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 0, '2020-04-05 07:30:21', '::1');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `mcustomer`
--
ALTER TABLE `mcustomer`
  ADD PRIMARY KEY (`ID_Customer`);

--
-- Indeks untuk tabel `mmechanic`
--
ALTER TABLE `mmechanic`
  ADD PRIMARY KEY (`ID_Mechanic`);

--
-- Indeks untuk tabel `mservice`
--
ALTER TABLE `mservice`
  ADD PRIMARY KEY (`ID_Service`);

--
-- Indeks untuk tabel `mstatus`
--
ALTER TABLE `mstatus`
  ADD PRIMARY KEY (`ID_Status`);

--
-- Indeks untuk tabel `mvehicle`
--
ALTER TABLE `mvehicle`
  ADD PRIMARY KEY (`ID_Vehicle`),
  ADD KEY `FK_TYPE` (`ID_Type`),
  ADD KEY `FK_CUSTOMER` (`ID_Customer`);

--
-- Indeks untuk tabel `mvehicletype`
--
ALTER TABLE `mvehicletype`
  ADD PRIMARY KEY (`ID_Type`);

--
-- Indeks untuk tabel `postcategories`
--
ALTER TABLE `postcategories`
  ADD PRIMARY KEY (`PostCategoryID`);

--
-- Indeks untuk tabel `postimages`
--
ALTER TABLE `postimages`
  ADD PRIMARY KEY (`PostImageID`);

--
-- Indeks untuk tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`PostID`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`SettingID`);

--
-- Indeks untuk tabel `torder`
--
ALTER TABLE `torder`
  ADD PRIMARY KEY (`ID_Order`),
  ADD KEY `FK_ORDER_STATUS` (`ID_Status`),
  ADD KEY `FK_ORDER_CUST` (`ID_Customer`),
  ADD KEY `FK_ORDER_VEHICLE` (`ID_Vehicle`),
  ADD KEY `FK_ORDER_SERVICE` (`ID_Service`),
  ADD KEY `FK_ORDER_MECHANIC` (`ID_Mechanic`);

--
-- Indeks untuk tabel `torder_status`
--
ALTER TABLE `torder_status`
  ADD PRIMARY KEY (`Uniq`),
  ADD KEY `FK_ORDER` (`ID_Order`),
  ADD KEY `FK_STATUS` (`ID_Status`);

--
-- Indeks untuk tabel `userinformation`
--
ALTER TABLE `userinformation`
  ADD PRIMARY KEY (`UserName`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserName`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `mcustomer`
--
ALTER TABLE `mcustomer`
  MODIFY `ID_Customer` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `mmechanic`
--
ALTER TABLE `mmechanic`
  MODIFY `ID_Mechanic` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `mservice`
--
ALTER TABLE `mservice`
  MODIFY `ID_Service` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `mstatus`
--
ALTER TABLE `mstatus`
  MODIFY `ID_Status` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `mvehicle`
--
ALTER TABLE `mvehicle`
  MODIFY `ID_Vehicle` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `mvehicletype`
--
ALTER TABLE `mvehicletype`
  MODIFY `ID_Type` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `postcategories`
--
ALTER TABLE `postcategories`
  MODIFY `PostCategoryID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `postimages`
--
ALTER TABLE `postimages`
  MODIFY `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `PostID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `RoleID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `SettingID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `torder`
--
ALTER TABLE `torder`
  MODIFY `ID_Order` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `torder_status`
--
ALTER TABLE `torder_status`
  MODIFY `Uniq` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `mvehicle`
--
ALTER TABLE `mvehicle`
  ADD CONSTRAINT `FK_CUSTOMER` FOREIGN KEY (`ID_Customer`) REFERENCES `mcustomer` (`ID_Customer`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_TYPE` FOREIGN KEY (`ID_Type`) REFERENCES `mvehicletype` (`ID_Type`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `torder`
--
ALTER TABLE `torder`
  ADD CONSTRAINT `FK_ORDER_CUST` FOREIGN KEY (`ID_Customer`) REFERENCES `mcustomer` (`ID_Customer`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ORDER_SERVICE` FOREIGN KEY (`ID_Service`) REFERENCES `mservice` (`ID_Service`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ORDER_STATUS` FOREIGN KEY (`ID_Status`) REFERENCES `mstatus` (`ID_Status`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ORDER_VEHICLE` FOREIGN KEY (`ID_Vehicle`) REFERENCES `mvehicle` (`ID_Vehicle`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `torder_status`
--
ALTER TABLE `torder_status`
  ADD CONSTRAINT `FK_ORDER` FOREIGN KEY (`ID_Order`) REFERENCES `torder` (`ID_Order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_STATUS` FOREIGN KEY (`ID_Status`) REFERENCES `mstatus` (`ID_Status`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
