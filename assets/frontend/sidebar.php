<div class="col-md-4">
    <div class="box box-solid">
        <div class="box-header with-border">
            <i class="fa fa-image"></i>
            <h3 class="box-title">Galeri</h3>
        </div>
        <div class="box-body">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                    <!--<li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>-->
                </ol>
                <div class="carousel-inner">
                    <!--<div class="item active">
                                        <img src="<?=MY_IMAGEURL?>/slide/1.jpg" alt="SIDAMA">

                                        <div class="carousel-caption">
                                            First Slide
                                        </div>
                                    </div>-->
                    <div class="item active">
                        <img src="<?=MY_IMAGEURL?>/slide/2.jpeg" alt="SIDAMA">
                    </div>
                    <div class="item">
                        <img src="<?=MY_IMAGEURL?>/slide/3.jpeg" alt="SIDAMA">
                    </div>
                    <div class="item">
                        <img src="<?=MY_IMAGEURL?>/slide/4.jpeg" alt="SIDAMA">
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="box box-solid">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-th"></i>
            <h3 class="box-title">Statistik Pengunjung</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body border-radius-none">
            <div class="chart" id="visitor-chart" style="height: 250px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">

            </div>
        </div>
    </div>

    <div class="box box-solid bg-yellow-gradient">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-calendar"></i>
            <h3 class="box-title">Kalender</h3>

            <div class="pull-right box-tools">
                <button type="button" class="btn btn-warning btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="box-body no-padding" style="display: block;">
            <!--The calendar -->
            <div id="calendar" style="width: 100%">
            </div>
        </div>
    </div>
</div>