<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/2/2019
 * Time: 12:48 AM
 */
class Project extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
        /*if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLE_PM && GetLoggedUser()[COL_ROLEID] != ROLE_PMO) {
            redirect('user/dashboard');
        }*/
    }

    function index() {
        $ruser = GetLoggedUser();
        $data['title'] = "Projects";
        $q = $this->db
            ->select("
            tproject.*,
            mcategory.NM_Category,
            mcustomer.NM_Customer,
            mstatus.NM_Status,
            mstatus.NM_LabelColor,
            pm.NM_Employee as NM_PM,
            pmo.NM_Employee as NM_PMO,
            (select sum(t.Mandays_Consumed) from tproject_task t where t.ID_Project = tproject.ID_Project and t.ID_Status = ".STATUS_TASK_DONE.") as Mandays_Consumed")
            ->join(TBL_MCATEGORY,TBL_MCATEGORY.'.'.COL_ID_CATEGORY." = ".TBL_TPROJECT.".".COL_ID_CATEGORY,"left")
            ->join(TBL_MCUSTOMER,TBL_MCUSTOMER.'.'.COL_ID_CUSTOMER." = ".TBL_TPROJECT.".".COL_ID_CUSTOMER,"left")
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TPROJECT.".".COL_ID_STATUS,"left")
            ->join(TBL_MEMPLOYEE." as pm",'pm.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT.".".COL_ID_PM,"left")
            ->join(TBL_MEMPLOYEE." as pmo",'pmo.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT.".".COL_ID_PMO,"left");
        if($ruser[COL_ROLEID] == ROLE_PM || $ruser[COL_ROLEID] == ROLE_PMO) {
            $q->where("(pm.".COL_ID_EMPLOYEE." = ".$ruser[COL_COMPANYID]." or pmo.".COL_ID_EMPLOYEE." = ".$ruser[COL_COMPANYID].")");
        }
        else if($ruser[COL_ROLEID] == ROLE_EMPLOYEE) {
            $q->where("EXISTS (select emp.ID_Employee from tproject_task t left join tproject_employee emp on emp.ID_Task = t.ID_Task where t.ID_Project = tproject.ID_Project and emp.ID_Employee = ".$ruser[COL_COMPANYID].")");
        }
        if(!empty($_GET)) {
            $data['filter'] = $_GET;
            $arrCond = array();
            if(!empty($_GET[COL_ID_CATEGORY])) $arrCond[TBL_TPROJECT.".".COL_ID_CATEGORY] = $_GET[COL_ID_CATEGORY];
            if(!empty($_GET[COL_ID_STATUS])) $arrCond[TBL_TPROJECT.".".COL_ID_STATUS] = $_GET[COL_ID_STATUS];
            if(!empty($_GET[COL_ID_CUSTOMER])) $arrCond[TBL_TPROJECT.".".COL_ID_CUSTOMER] = $_GET[COL_ID_CUSTOMER];
            if(!empty($_GET[COL_NM_PROJECT])) $arrCond[TBL_TPROJECT.".".COL_NM_PROJECT." like "] = "%".$_GET[COL_NM_PROJECT]."%";

            if(count($arrCond) > 0) {
                $q->where($arrCond);
            }
        }
        if(!empty($_GET["SortCol"])) {
            $q->order_by(TBL_TPROJECT.".".$_GET["SortCol"],(!empty($_GET["SortDir"])?$_GET["SortDir"]:'ASC'));
        } else {
            $q->order_by(TBL_TPROJECT.".".COL_CREATEDON,(!empty($_GET["SortDir"])?$_GET["SortDir"]:'DESC'));
        }
        $data['res'] = $q->get(TBL_TPROJECT)->result_array();
        //echo $this->db->last_query();
        //return;
        $this->load->view('project/index_project', $data);
    }

    function add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLE_PM) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Projects";
        $data['edit'] = FALSE;
        $data['data'] = array(COL_ID_STATUS => GetDefaultStatus());

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_ID_CATEGORY => $this->input->post(COL_ID_CATEGORY),
                COL_ID_CUSTOMER => $this->input->post(COL_ID_CUSTOMER),
                COL_ID_STATUS => $this->input->post(COL_ID_STATUS),
                COL_ID_PM => $this->input->post(COL_ID_PM),
                COL_ID_PMO => $this->input->post(COL_ID_PMO),
                COL_NM_PROJECT => $this->input->post(COL_NM_PROJECT),
                COL_NM_PROJECTDESC => $this->input->post(COL_NM_PROJECTDESC),
                //COL_NM_ATTACHMENT => $this->input->post(COL_NM_EMPLOYEE),
                COL_MANDAYS => $this->input->post(COL_MANDAYS),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            if($user[COL_ROLEID]==ROLE_PM) {
                $rec[COL_ID_PM] = $user[COL_COMPANYID];
            }

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 4000;
            $config['max_height']  = 4000;
            $config['overwrite'] = FALSE;

            $this->load->library('upload',$config);
            if(!empty($_FILES['userfile']['name'])) {
                // Upload file to server
                if($this->upload->do_upload()){
                    $fileData = $this->upload->data();
                    $rec[COL_NM_ATTACHMENT] = $fileData['file_name'];
                }
            }

            $res = $this->db->insert(TBL_TPROJECT, $rec);

            if($res) {
                redirect('project/detail/'.$this->db->insert_id());
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('project/form_project', $data);
        }
    }

    function edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLE_PM) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_ID_PROJECT, $id)->get(TBL_TPROJECT)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Projects";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_ID_CATEGORY => $this->input->post(COL_ID_CATEGORY),
                COL_ID_CUSTOMER => $this->input->post(COL_ID_CUSTOMER),
                COL_ID_STATUS => $this->input->post(COL_ID_STATUS),
                COL_ID_PM => $this->input->post(COL_ID_PM),
                COL_ID_PMO => $this->input->post(COL_ID_PMO),
                COL_NM_PROJECT => $this->input->post(COL_NM_PROJECT),
                COL_NM_PROJECTDESC => $this->input->post(COL_NM_PROJECTDESC),
                //COL_NM_ATTACHMENT => $this->input->post(COL_NM_EMPLOYEE),
                COL_MANDAYS => $this->input->post(COL_MANDAYS),

                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 4000;
            $config['max_height']  = 4000;
            $config['overwrite'] = FALSE;

            $this->load->library('upload',$config);
            if(!empty($_FILES['userfile']['name'])) {
                // Upload file to server
                if($this->upload->do_upload()){
                    $fileData = $this->upload->data();
                    $rec[COL_NM_ATTACHMENT] = $fileData['file_name'];
                }
            }

            $res = $this->db->where(COL_ID_PROJECT, $id)->update(TBL_TPROJECT, $rec);
            if($res) {
                redirect('project/detail/'.$id);
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('project/form_project', $data);
        }
    }

    function delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLE_PM && GetLoggedUser()[COL_ROLEID] != ROLE_PMO) {
            redirect('user/dashboard');
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_TPROJECT, array(COL_ID_PROJECT => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function detail($id) {
        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db
            ->select("tproject.*, mcategory.NM_Category, mcustomer.NM_Customer, mstatus.NM_Status, mstatus.NM_LabelColor, pm.NM_Employee as NM_PM, pmo.NM_Employee as NM_PMO")
            ->join(TBL_MCATEGORY,TBL_MCATEGORY.'.'.COL_ID_CATEGORY." = ".TBL_TPROJECT.".".COL_ID_CATEGORY,"left")
            ->join(TBL_MCUSTOMER,TBL_MCUSTOMER.'.'.COL_ID_CUSTOMER." = ".TBL_TPROJECT.".".COL_ID_CUSTOMER,"left")
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TPROJECT.".".COL_ID_STATUS,"left")
            ->join(TBL_MEMPLOYEE." as pm",'pm.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT.".".COL_ID_PM,"left")
            ->join(TBL_MEMPLOYEE." as pmo",'pmo.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT.".".COL_ID_PMO,"left")
            ->where(COL_ID_PROJECT, $id)->get(TBL_TPROJECT)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Projects";
        $this->load->view('project/form_project_detail', $data);
    }

    function tasks($id) {
        $data['data'] = array(COL_ID_PROJECT=>$id);
        if($this->input->is_ajax_request()) {
            $this->load->view('project/index_project_task_partial', $data);
        } else {
            echo "Under development.";
        }
    }
}