<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/22/2019
 * Time: 5:42 PM
 */
class Report extends MY_Controller {
    function __construct() {
        parent::__construct();
        $ruser = GetLoggedUser();
        if(!IsLogin()) {
            redirect('user/login');
        }
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLE_PM && $ruser[COL_ROLEID] != ROLE_PMO) {
            redirect('user/dashboard');
        }
    }

    function project_mandays() {
        $ruser = GetLoggedUser();
        $data['title'] = "Report - Project Mandays";
        $data['edit'] = false;
        $data['cetak'] = $cetak = $this->input->get("cetak");

        $q = $this->db
            ->select("
            tproject.*,
            mcategory.NM_Category,
            mcustomer.NM_Customer,
            mstatus.NM_Status,
            mstatus.NM_LabelColor,
            pm.NM_Employee as NM_PM,
            pmo.NM_Employee as NM_PMO,
            (select sum(t.Mandays_Consumed) from tproject_task t where t.ID_Project = tproject.ID_Project and t.ID_Status = ".STATUS_TASK_DONE.") as Mandays_Consumed")
            ->join(TBL_MCATEGORY,TBL_MCATEGORY.'.'.COL_ID_CATEGORY." = ".TBL_TPROJECT.".".COL_ID_CATEGORY,"left")
            ->join(TBL_MCUSTOMER,TBL_MCUSTOMER.'.'.COL_ID_CUSTOMER." = ".TBL_TPROJECT.".".COL_ID_CUSTOMER,"left")
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TPROJECT.".".COL_ID_STATUS,"left")
            ->join(TBL_MEMPLOYEE." as pm",'pm.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT.".".COL_ID_PM,"left")
            ->join(TBL_MEMPLOYEE." as pmo",'pmo.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT.".".COL_ID_PMO,"left");
        if($ruser[COL_ROLEID] == ROLE_PM || $ruser[COL_ROLEID] == ROLE_PMO) {
            $q->where("(pm.".COL_ID_EMPLOYEE." = ".$ruser[COL_COMPANYID]." or pmo.".COL_ID_EMPLOYEE." = ".$ruser[COL_COMPANYID].")");
        }
        /*else if($ruser[COL_ROLEID] == ROLE_EMPLOYEE) {
            $q->where("EXISTS (select emp.ID_Employee from tproject_task t left join tproject_employee emp on emp.ID_Task = t.ID_Task where t.ID_Project = tproject.ID_Project and emp.ID_Employee = ".$ruser[COL_COMPANYID].")");
        }*/
        if(!empty($_GET)) {
            $data['filter'] = $_GET;
            $arrCond = array();
            if(!empty($_GET[COL_ID_CATEGORY])) $arrCond[TBL_TPROJECT.".".COL_ID_CATEGORY] = $_GET[COL_ID_CATEGORY];
            if(!empty($_GET[COL_ID_STATUS])) $arrCond[TBL_TPROJECT.".".COL_ID_STATUS] = $_GET[COL_ID_STATUS];
            if(!empty($_GET[COL_ID_CUSTOMER])) $arrCond[TBL_TPROJECT.".".COL_ID_CUSTOMER] = $_GET[COL_ID_CUSTOMER];
            if(!empty($_GET[COL_NM_PROJECT])) $arrCond[TBL_TPROJECT.".".COL_NM_PROJECT." like "] = "%".$_GET[COL_NM_PROJECT]."%";

            if(count($arrCond) > 0) {
                $q->where($arrCond);
            }
        }
        if(!empty($_GET["SortCol"])) {
            $q->order_by(TBL_TPROJECT.".".$_GET["SortCol"],(!empty($_GET["SortDir"])?$_GET["SortDir"]:'ASC'));
        } else {
            $q->order_by(TBL_TPROJECT.".".COL_CREATEDON,(!empty($_GET["SortDir"])?$_GET["SortDir"]:'DESC'));
        }
        $data['res'] = $q->get(TBL_TPROJECT)->result_array();
        $this->load->view('report/project_mandays', $data);
    }

    function employee_occupation() {
        $ruser = GetLoggedUser();
        $data['title'] = "Report - Employee Occupation";
        $data['edit'] = false;
        $data['cetak'] = $cetak = $this->input->get("cetak");

        $q = @"
        SELECT
        em.NM_NIK,
        em.NM_Employee,
        p.NM_Project,
        COUNT(t.ID_Task) AS Task
        FROM tproject_employee e
        INNER JOIN tproject_task t ON t.ID_Task = e.ID_Task
        INNER JOIN tproject p ON p.ID_Project = t.ID_Project
        INNER JOIN memployee em ON em.ID_Employee = e.ID_Employee
        WHERE
            t.ID_Status != 8
        GROUP BY em.ID_Employee, p.ID_Project
        ";

        $data['res'] = $this->db->query($q)->result_array();
        $this->load->view('report/employee_occupation', $data);
    }
}