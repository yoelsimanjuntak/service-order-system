<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/3/2019
 * Time: 12:40 PM
 */
class Task extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function index() {
        $ruser = GetLoggedUser();
        $data['title'] = "Tasks";
        $q = $this->db
            ->select("tproject_task.*, tproject.NM_Project, mstatus.NM_Status, mstatus.NM_LabelColor, mpriority.NM_Priority")
            ->join(TBL_TPROJECT,TBL_TPROJECT.'.'.COL_ID_PROJECT." = ".TBL_TPROJECT_TASK.".".COL_ID_PROJECT,"left")
            ->join(TBL_MPRIORITY,TBL_MPRIORITY.'.'.COL_ID_PRIORITY." = ".TBL_TPROJECT_TASK.".".COL_ID_PRIORITY,"left")
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TPROJECT_TASK.".".COL_ID_STATUS,"left");
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $q->where("EXISTS (select emp.ID_Employee from tproject_employee emp where emp.ID_Task = tproject_task.ID_Task and emp.ID_Employee = ".$ruser[COL_COMPANYID].")");
        }
        if(!empty($_GET)) {
            $data['filter'] = $_GET;
            $arrCond = array();
            if(!empty($_GET[COL_ID_PRIORITY])) $arrCond[TBL_TPROJECT_TASK.".".COL_ID_PRIORITY] = $_GET[COL_ID_PRIORITY];
            if(!empty($_GET[COL_ID_STATUS])) $arrCond[TBL_TPROJECT_TASK.".".COL_ID_STATUS] = $_GET[COL_ID_STATUS];
            if(!empty($_GET[COL_NM_TASK])) $arrCond[TBL_TPROJECT_TASK.".".COL_NM_TASK." like "] = "%".$_GET[COL_NM_TASK]."%";
            if(!empty($_GET[COL_NM_PROJECT])) $arrCond[TBL_TPROJECT.".".COL_NM_PROJECT." like "] = "%".$_GET[COL_NM_PROJECT]."%";

            if(count($arrCond) > 0) {
                $q->where($arrCond);
            }
        }
        if(!empty($_GET["SortCol"])) {
            $q->order_by(TBL_TPROJECT_TASK.".".$_GET["SortCol"],(!empty($_GET["SortDir"])?$_GET["SortDir"]:'ASC'));
        } else {
            $q->order_by(TBL_TPROJECT_TASK.".".COL_CREATEDON,(!empty($_GET["SortDir"])?$_GET["SortDir"]:'DESC'));
        }
        $data['res'] = $q->get(TBL_TPROJECT_TASK)->result_array();
        $this->load->view('task/index_task', $data);
    }

    function add($id=null,$ajax=0) {
        $user = GetLoggedUser();
        $data['title'] = "Task";
        $data['edit'] = FALSE;
        $data['data'] = array(
            COL_ID_STATUS => GetDefaultStatus(),
            COL_ID_PROJECT => !empty($id)?$id:null,
            "IsAjax" => $ajax
        );

        if(!empty($_POST)){
            $ajax = $this->input->post("IsAjax");
            $emps = $this->input->post(COL_ID_EMPLOYEE);
            $data['data'] = $_POST;
            $rec = array(
                COL_ID_PROJECT => $this->input->post(COL_ID_PROJECT),
                COL_ID_PRIORITY => $this->input->post(COL_ID_PRIORITY),
                COL_ID_STATUS => $this->input->post(COL_ID_STATUS),
                COL_NM_TASK => $this->input->post(COL_NM_TASK),
                COL_NM_PLANNEDSTART => date("Y-m-d", strtotime($this->input->post(COL_NM_PLANNEDSTART))),
                COL_NM_PLANNEDEND => date("Y-m-d", strtotime($this->input->post(COL_NM_PLANNEDEND))),
                COL_MANDAYS_CONSUMED => $this->input->post(COL_MANDAYS_CONSUMED),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 4000;
            $config['max_height']  = 4000;
            $config['overwrite'] = FALSE;

            $this->load->library('upload',$config);
            if(!empty($_FILES['userfile']['name'])) {
                if($this->upload->do_upload()){
                    $fileData = $this->upload->data();
                    $rec[COL_NM_ATTACHMENT] = $fileData['file_name'];
                }
            }

            $this->db->trans_begin();
            try {
                $res = $this->db->insert(TBL_TPROJECT_TASK, $rec);
                $res2 = true;
                $projectid = $this->db->insert_id();

                if(!empty($emps) && $emps > 0) {
                    $recEmps = array();
                    foreach($emps as $e) {
                        $recEmps[] = array(COL_ID_TASK=>$projectid, COL_ID_EMPLOYEE=>$e);
                    }
                    if(count($recEmps) > 0) {
                        $res2 = $this->db->insert_batch(TBL_TPROJECT_EMPLOYEE, $recEmps);
                    }
                }

                if($res && $res2) {
                    $this->db->trans_commit();
                    if($ajax) {
                        echo json_encode(array("error"=>0));
                        return;
                    } else {
                        redirect('task/index');
                    }
                } else {
                    $this->db->trans_rollback();
                    if($ajax) {
                        echo json_encode(array("error"=>"Server Error."));
                        return;
                    } else {
                        redirect(current_url()."?error=1");
                    }
                }
            } catch(Exception $e) {
                $this->db->trans_rollback();
                if($ajax) {
                    echo json_encode(array("error"=>"Server Error."));
                    return;
                } else {
                    redirect(current_url()."?error=1");
                }
            }
        }
        else {
            if($this->input->is_ajax_request()) {
                $this->load->view('task/form_task_partial', $data);
            }
        }

    }

    function edit($id,$ajax=0) {
        $user = GetLoggedUser();
        $data['title'] = "Task";
        $data['edit'] = TRUE;

        $rdata = $data['data'] = $this->db
            ->select("tproject_task.*, mpriority.NM_Priority, mstatus.NM_Status, mstatus.NM_LabelColor, (select GROUP_CONCAT(ID_Employee) AS emps from tproject_employee where ID_Task = tproject_task.ID_Task) as ID_Employee")
            ->join(TBL_MPRIORITY,TBL_MPRIORITY.'.'.COL_ID_PRIORITY." = ".TBL_TPROJECT_TASK.".".COL_ID_PRIORITY,"left")
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TPROJECT_TASK.".".COL_ID_STATUS,"left")
            ->where(COL_ID_TASK, $id)
            ->get(TBL_TPROJECT_TASK)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['data']['IsAjax'] = $ajax;
        if(!empty($_POST)){
            $ajax = $this->input->post("IsAjax");
            $emps = $this->input->post(COL_ID_EMPLOYEE);
            $data['data'] = $_POST;
            $rec = array(
                COL_ID_PROJECT => $this->input->post(COL_ID_PROJECT),
                COL_ID_PRIORITY => $this->input->post(COL_ID_PRIORITY),
                COL_ID_STATUS => $this->input->post(COL_ID_STATUS),
                COL_NM_TASK => $this->input->post(COL_NM_TASK),
                COL_NM_PLANNEDSTART => date("Y-m-d", strtotime($this->input->post(COL_NM_PLANNEDSTART))),
                COL_NM_PLANNEDEND => date("Y-m-d", strtotime($this->input->post(COL_NM_PLANNEDEND))),
                COL_MANDAYS_CONSUMED => $this->input->post(COL_MANDAYS_CONSUMED),

                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 4000;
            $config['max_height']  = 4000;
            $config['overwrite'] = FALSE;

            $this->load->library('upload',$config);
            if(!empty($_FILES['userfile']['name'])) {
                if($this->upload->do_upload()){
                    $fileData = $this->upload->data();
                    $rec[COL_NM_ATTACHMENT] = $fileData['file_name'];
                }
            }

            $this->db->trans_begin();
            try {
                $res = $this->db->where(COL_ID_TASK, $id)->update(TBL_TPROJECT_TASK, $rec);
                $res2 = $this->db->delete(TBL_TPROJECT_EMPLOYEE, array(COL_ID_TASK => $id));
                $res3 = true;

                if(!empty($emps) && $emps > 0) {
                    $recEmps = array();
                    foreach($emps as $e) {
                        $recEmps[] = array(COL_ID_TASK=>$id, COL_ID_EMPLOYEE=>$e);
                    }
                    if(count($recEmps) > 0) {
                        $res3 = $this->db->insert_batch(TBL_TPROJECT_EMPLOYEE, $recEmps);
                    }
                }

                if($res && $res2 && $res3) {
                    $this->db->trans_commit();
                    if($ajax) {
                        echo json_encode(array("error"=>0));
                        return;
                    } else {
                        redirect('task/index');
                    }
                } else {
                    $this->db->trans_rollback();
                    if($ajax) {
                        echo json_encode(array("error"=>"Server Error."));
                        return;
                    } else {
                        redirect(current_url()."?error=1");
                    }
                }
            } catch(Exception $e) {
                $this->db->trans_rollback();
                if($ajax) {
                    echo json_encode(array("error"=>"Server Error."));
                    return;
                } else {
                    redirect(current_url()."?error=1");
                }
            }
        } else {
            if($this->input->is_ajax_request()) {
                $this->load->view('task/form_task_partial', $data);
            }
        }
    }

    function delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_TPROJECT_TASK, array(COL_ID_TASK => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function detail($id) {
        $user = GetLoggedUser();

        $q = $this->db
            ->select("tproject_task.*, tproject.NM_Project, mpriority.NM_Priority, mstatus.NM_Status, mstatus.NM_LabelColor")
            ->join(TBL_TPROJECT,TBL_TPROJECT.'.'.COL_ID_PROJECT." = ".TBL_TPROJECT_TASK.".".COL_ID_PROJECT,"left")
            ->join(TBL_MPRIORITY,TBL_MPRIORITY.'.'.COL_ID_PRIORITY." = ".TBL_TPROJECT_TASK.".".COL_ID_PRIORITY,"left")
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TPROJECT_TASK.".".COL_ID_STATUS,"left")
            ->where(COL_ID_TASK, $id);
        if($user[COL_ROLEID] != ROLEADMIN) {
            $q->where("EXISTS (select emp.ID_Employee from tproject_employee emp where emp.ID_Task = tproject_task.ID_Task and emp.ID_Employee = ".$user[COL_COMPANYID].")");
        }
        $rdata = $data['data'] = $q->get(TBL_TPROJECT_TASK)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Task";
        $this->load->view('task/form_task_detail', $data);
    }

    function activity($id) {
        $res = $this->db
            ->join(TBL_MEMPLOYEE,TBL_MEMPLOYEE.'.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT_ACTIVITY.".".COL_ID_EMPLOYEE,"left")
            ->where(COL_ID_TASK, $id)
            ->order_by(TBL_TPROJECT_ACTIVITY.".".COL_CREATEDON, 'desc')
            ->get(TBL_TPROJECT_ACTIVITY)
            ->result_array();

        if(count($res) > 0) {
            $html = '<div class="timeline">';
            $html .= '<div class="time-label"></div>';
            foreach($res as $r) {
                $html .= '<div><i class="fas fa-comment bg-warning"></i>';
                $html .= '<div class="timeline-item">';
                $html .= '<span class="time"><i class="far fa-clock"></i> '.date('D, d-m-Y H:i', strtotime($r[COL_CREATEDON])).'</span>';
                $html .= '<h3 class="timeline-header">'.$r[COL_NM_EMPLOYEE].'</h3>';
                $html .= '<div class="timeline-body"><p>'.$r[COL_NM_ACTIVITY].'</p></div>';
                if(!empty($r[COL_NM_ATTACHMENT])) {
                    $html .= '<div class="timeline-footer pt-0"><a href="'.MY_UPLOADURL.$r[COL_NM_ATTACHMENT].'" target="_blank" class="link-black text-sm" style="color: #6c757d !important"><i class="fa fa-paperclip"></i> '.$r[COL_NM_ATTACHMENT].'</a></div>';
                }

                $html .= '</div>';
                $html .= '</div>';
            }
            $html .= '<div><i class="far fa-clock bg-gray"></i></div>';
            $html .= '</div>';
            echo $html;
        } else {
            //echo "<p class='p-2'>No record found.</p>";
        }
    }

    function activity_add() {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $rec = array(
                COL_ID_TASK => $this->input->post(COL_ID_TASK),
                COL_ID_EMPLOYEE => $user[COL_COMPANYID],
                COL_NM_ACTIVITY => $this->input->post(COL_NM_ACTIVITY),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 4000;
            $config['max_height']  = 4000;
            $config['overwrite'] = FALSE;

            $this->load->library('upload',$config);
            if(!empty($_FILES['userfile']['name'])) {
                if($this->upload->do_upload()){
                    $fileData = $this->upload->data();
                    $rec[COL_NM_ATTACHMENT] = $fileData['file_name'];
                }
            }

            $res = $this->db->insert(TBL_TPROJECT_ACTIVITY, $rec);
            if($res) {
                echo json_encode(array("error"=>0));
                return;
            } else {
                echo json_encode(array("error"=>"Server Error."));
                return;
            }
        }

    }

    function change_status($id) {
        $user = GetLoggedUser();
        $rtask = $this->db
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TPROJECT_TASK.".".COL_ID_STATUS,"left")
            ->where(COL_ID_TASK, $id)
            ->get(TBL_TPROJECT_TASK)
            ->row_array();
        if(empty($rtask)) {
            echo json_encode(array("error"=>"Invalid Task ID."));
            return;
        }

        if(!empty($_POST)){
            $rstatus = $this->db->where(COL_ID_STATUS, $this->input->post(COL_ID_STATUS))->get(TBL_MSTATUS)->row_array();
            if(empty($rstatus)) {
                echo json_encode(array("error"=>"Invalid Status ID."));
                return;
            }
            $rec = array(
                COL_ID_TASK => $id,
                COL_ID_PROJECT => $rtask[COL_ID_PROJECT],
                COL_ID_EMPLOYEE => $user[COL_COMPANYID],
                COL_NM_LOG => "Change Status",
                COL_NM_VALUEFROM => $rtask[COL_NM_STATUS],
                COL_NM_VALUETO => $rstatus[COL_NM_STATUS],

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_TPROJECT_LOG, $rec);
            if($res) {
                $this->db->where(COL_ID_TASK, $id)->update(TBL_TPROJECT_TASK, array(COL_ID_STATUS => $this->input->post(COL_ID_STATUS)));
                echo json_encode(array("error"=>0));
                return;
            } else {
                echo json_encode(array("error"=>"Server Error."));
                return;
            }
        }

    }

    function log($id) {
        $res = $this->db
            ->join(TBL_MEMPLOYEE,TBL_MEMPLOYEE.'.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT_LOG.".".COL_ID_EMPLOYEE,"left")
            ->where(COL_ID_TASK, $id)
            ->order_by(TBL_TPROJECT_LOG.".".COL_CREATEDON, 'desc')
            ->get(TBL_TPROJECT_LOG)
            ->result_array();

        if(count($res) > 0) {
            $html = '<div class="timeline">';
            $html .= '<div class="time-label"></div>';
            foreach($res as $r) {
                $html .= '<div><i class="fas fa-clock bg-primary"></i>';
                $html .= '<div class="timeline-item">';
                $html .= '<span class="time"><i class="far fa-clock"></i> '.date('D, d-m-Y H:i', strtotime($r[COL_CREATEDON])).'</span>';
                $html .= '<h3 class="timeline-header">'.$r[COL_NM_EMPLOYEE].'</h3>';
                $html .= '<div class="timeline-body"><p>'.$r[COL_NM_LOG].' from <b>'.$r[COL_NM_VALUEFROM].'</b> to <b>'.$r[COL_NM_VALUETO].'</b>.</p></div>';

                $html .= '</div>';
                $html .= '</div>';
            }
            $html .= '<div><i class="far fa-clock bg-gray"></i></div>';
            $html .= '</div>';
            echo $html;
        } else {
            echo "No record found.";
        }
    }
}