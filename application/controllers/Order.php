<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 30/01/2020
 * Time: 10:39
 */
class Order extends MY_Controller {
    function __construct() {
        parent::__construct();
    }

    function index() {
      if(!IsLogin()) {
          redirect('user/login');
      }

      $ruser = GetLoggedUser();
      $data['filter'] = $arrfilter = array(
        'DateFrom' => $this->input->get('DateFrom') ? $this->input->get('DateFrom') : date('Y-m-').'01',
        'DateTo' => $this->input->get('DateTo') ? $this->input->get('DateTo') : date('Y-m-d'),
        COL_ID_STATUS => $this->input->get(COL_ID_STATUS) ? $this->input->get(COL_ID_STATUS) : '',
        COL_ID_ORDER => $this->input->get(COL_ID_ORDER) ? $this->input->get(COL_ID_ORDER) : '',
        'Keyword' => $this->input->get('Keyword') ? $this->input->get('Keyword') : ''
      );
      $data['title'] = "Registrasi";

      if($ruser[COL_ROLEID] != ROLEADMIN) {
        $this->db->where(TBL_TORDER.'.'.COL_ID_CUSTOMER, $ruser[COL_COMPANYID]);
      }
      $this->db->where(TBL_TORDER.'.'.COL_DATE.' >= ', $arrfilter['DateFrom']);
      $this->db->where(TBL_TORDER.'.'.COL_DATE.' <= ', $arrfilter['DateTo']);
      if(!empty($arrfilter[COL_ID_STATUS])) {
        $this->db->where(TBL_TORDER.'.'.COL_ID_STATUS, $arrfilter[COL_ID_STATUS]);
      }
      if(!empty($arrfilter[COL_ID_ORDER])) {
        $this->db->like(TBL_TORDER.'.'.COL_ID_ORDER, ltrim($arrfilter[COL_ID_ORDER],'0'));
      }
      if(!empty($arrfilter['Keyword'])) {
        $this->db->group_start();
        $this->db->like(TBL_MMECHANIC.'.'.COL_NM_MECHANIC, $arrfilter['Keyword']);
        $this->db->or_like(TBL_MCUSTOMER.'.'.COL_NM_CUSTOMER, $arrfilter['Keyword']);
        $this->db->or_like(TBL_MVEHICLE.'.'.COL_NO_PLAT, $arrfilter['Keyword']);
        $this->db->or_like(TBL_MVEHICLETYPE.'.'.COL_NM_TYPE, $arrfilter['Keyword']);
        $this->db->group_end();
      }
      $data['res'] = $this->db
          ->select('*, torder.CreatedBy as CreatedBy, torder.CreatedOn as CreatedOn')
          ->join(TBL_MSERVICE,TBL_MSERVICE.'.'.COL_ID_SERVICE." = ".TBL_TORDER.".".COL_ID_SERVICE,"left")
          ->join(TBL_MCUSTOMER,TBL_MCUSTOMER.'.'.COL_ID_CUSTOMER." = ".TBL_TORDER.".".COL_ID_CUSTOMER,"left")
          ->join(TBL_MVEHICLE,TBL_MVEHICLE.'.'.COL_ID_VEHICLE." = ".TBL_TORDER.".".COL_ID_VEHICLE,"left")
          ->join(TBL_MVEHICLETYPE,TBL_MVEHICLETYPE.'.'.COL_ID_TYPE." = ".TBL_MVEHICLE.".".COL_ID_TYPE,"left")
          ->join(TBL_MMECHANIC,TBL_MMECHANIC.'.'.COL_ID_MECHANIC." = ".TBL_TORDER.".".COL_ID_MECHANIC,"left")
          ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TORDER.".".COL_ID_STATUS,"left")
          ->get(TBL_TORDER)
          ->result_array();
      $this->load->view('order/index', $data);
    }

    function add() {
        $ruser = GetLoggedUser();
        if(!IsLogin() && $ruser[COL_ROLEID] != ROLEADMIN) {
            redirect('user/login');
        }

        $user = GetLoggedUser();
        $data['title'] = "Work Order";
        $data['edit'] = FALSE;

        if(!empty($_POST)) {
            $data['data'] = $_POST;
            $stat_ = $this->input->post(COL_ID_STATUS);
            $cust_ = $this->input->post(COL_ID_CUSTOMER);
            if(empty($stat_)) {
                $stat_ = STATUS_ORDER_MENUNGGU;
            }
            if($ruser[COL_ROLEID]==ROLECUSTOMER) {
                $cust_ = $ruser[COL_COMPANYID];
            }
            $rec = array(
                COL_ID_SERVICE => $this->input->post(COL_ID_SERVICE),
                COL_ID_CUSTOMER => $cust_,
                COL_ID_VEHICLE => $this->input->post(COL_ID_VEHICLE),
                COL_ID_MECHANIC => $this->input->post(COL_ID_MECHANIC),
                COL_ID_STATUS => $stat_,
                COL_DATE => $this->input->post(COL_DATE),
                COL_NO_TELP => $this->input->post(COL_NO_TELP),
                COL_RE_CUSTOMER => $this->input->post(COL_RE_CUSTOMER),
                COL_RE_MECHANIC => $this->input->post(COL_RE_MECHANIC),
                COL_REMARKS => $this->input->post(COL_REMARKS),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            $this->db->trans_begin();
            try {
                $res = $this->db->insert(TBL_TORDER, $rec);

                $id_order = $this->db->insert_id();
                $stat = array(
                    COL_ID_ORDER => $id_order,
                    COL_ID_STATUS => $stat_,
                );
                $res2 = $this->db->insert(TBL_TORDER_STATUS, $stat);

                if($res && $res2) {
                    $this->db->trans_commit();
                    if($ruser[COL_ROLEID]==ROLEADMIN) redirect('order/index');
                    else redirect('home/index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            } catch(Exception $ex) {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('order/form', $data);
        }
    }

    function edit($id) {
        $user = GetLoggedUser();
        if(!IsLogin() && $user[COL_ROLEID] != ROLEADMIN) {
            redirect('user/login');
        }

        $rdata = $data['data'] = $this->db->where(COL_ID_ORDER, $id)->get(TBL_TORDER)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Work Order";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $stat_ = $this->input->post(COL_ID_STATUS);
            $cust_ = $this->input->post(COL_ID_CUSTOMER);
            if(empty($stat_)) {
                $stat_ = STATUS_ORDER_MENUNGGU;
            }
            if($user[COL_ROLEID]==ROLECUSTOMER) {
                $cust_ = $user[COL_COMPANYID];
            }
            $rec = array(
                COL_ID_SERVICE => $this->input->post(COL_ID_SERVICE),
                COL_ID_CUSTOMER => $cust_,
                COL_ID_VEHICLE => $this->input->post(COL_ID_VEHICLE),
                COL_ID_MECHANIC => $this->input->post(COL_ID_MECHANIC),
                COL_ID_STATUS => $stat_,
                COL_DATE => $this->input->post(COL_DATE),
                COL_NO_TELP => $this->input->post(COL_NO_TELP),
                COL_RE_CUSTOMER => $this->input->post(COL_RE_CUSTOMER),
                COL_RE_MECHANIC => $this->input->post(COL_RE_MECHANIC),
                COL_REMARKS => $this->input->post(COL_REMARKS),

                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );
            $this->db->trans_begin();
            try {
                $res = $this->db->where(COL_ID_ORDER, $id)->update(TBL_TORDER, $rec);
                $res2 = true;
                if($rec[COL_ID_STATUS] != $rdata[COL_ID_STATUS]) {
                    $stat = array(
                        COL_ID_ORDER => $id,
                        COL_ID_STATUS => $stat_,
                    );
                    $res2 = $this->db->insert(TBL_TORDER_STATUS, $stat);
                }
                if($res && $res2) {
                    $this->db->trans_commit();
                    if($user[COL_ROLEID]==ROLEADMIN) redirect('order/index');
                    else redirect('home/index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            } catch(Exception $ex) {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }

        }
        else {
            $this->load->view('order/form', $data);
        }
    }

    function delete(){
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_TORDER, array(COL_ID_ORDER => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function index_order_today($date) {
        $data['res'] = $this->db
            ->select('*, (select torder_status.Timestamp from torder_status where torder_status.ID_Order = torder.ID_Order and torder_status.ID_Status = '.STATUS_ORDER_PROSES.' order by torder_status.Timestamp desc limit 1) as START')
            ->join(TBL_MCUSTOMER,TBL_MCUSTOMER.'.'.COL_ID_CUSTOMER." = ".TBL_TORDER.".".COL_ID_CUSTOMER,"left")
            ->join(TBL_MVEHICLE,TBL_MVEHICLE.'.'.COL_ID_VEHICLE." = ".TBL_TORDER.".".COL_ID_VEHICLE,"left")
            ->join(TBL_MVEHICLETYPE,TBL_MVEHICLETYPE.'.'.COL_ID_TYPE." = ".TBL_MVEHICLE.".".COL_ID_TYPE,"left")
            ->join(TBL_MMECHANIC,TBL_MMECHANIC.'.'.COL_ID_MECHANIC." = ".TBL_TORDER.".".COL_ID_MECHANIC,"left")
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TORDER.".".COL_ID_STATUS,"left")
            ->join(TBL_MSERVICE,TBL_MSERVICE.'.'.COL_ID_SERVICE." = ".TBL_TORDER.".".COL_ID_SERVICE,"left")
            ->where(TBL_TORDER.".".COL_ID_STATUS." != ", STATUS_ORDER_SELESAI)
            ->where(TBL_TORDER.".".COL_DATE, $date)
            ->order_by(COL_ID_ORDER, 'desc')
            ->get(TBL_TORDER)
            ->result_array();
        $this->load->view('order/index_order_partial', $data);
    }

    function status_edit($id){
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          ShowJsonError('Akses ditolak.');
          return;
        }

        $ruser = GetLoggedUser();
        $rdata = $this->db->where(COL_ID_ORDER, $id)->get(TBL_TORDER)->row_array();
        if(empty($rdata)) {
          ShowJsonError('Order tidak valid.');
          return;
        }

        $stat_ = $this->input->post(COL_ID_STATUS);
        $this->db->trans_begin();
        try {
          $res = $this->db->where(COL_ID_ORDER, $id)->update(TBL_TORDER, array(
            COL_ID_STATUS => $stat_
          ));
          $resStat = true;
          if($rdata[COL_ID_STATUS] != $stat_) {
            $stat = array(
                COL_ID_ORDER => $id,
                COL_ID_STATUS => $stat_,
            );
            $resStat = $this->db->insert(TBL_TORDER_STATUS, $stat);
          }

          if($res && $resStat) {
            $this->db->trans_commit();
            ShowJsonSuccess('Status berhasil diubah.');
            return;
          } else {
            $this->db->trans_rollback();
            $this->db->trans_commit();
            ShowJsonError('Status gagal diubah.');
            return;
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError('Server Problem.');
          return;
        }
    }

    function mechanic_edit($id){
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
          ShowJsonError('Akses ditolak.');
          return;
        }

        $ruser = GetLoggedUser();
        $rdata = $this->db->where(COL_ID_ORDER, $id)->get(TBL_TORDER)->row_array();
        if(empty($rdata)) {
          ShowJsonError('Order tidak valid.');
          return;
        }

        $stat_ = $this->input->post(COL_ID_MECHANIC);
        $this->db->trans_begin();
        try {
          $res = $this->db->where(COL_ID_ORDER, $id)->update(TBL_TORDER, array(
            COL_ID_MECHANIC => $stat_
          ));

          if($res) {
            $this->db->trans_commit();
            ShowJsonSuccess('Mekanik berhasil diubah.');
            return;
          } else {
            $this->db->trans_rollback();
            $this->db->trans_commit();
            ShowJsonError('Status gagal diubah.');
            return;
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError('Server Problem.');
          return;
        }
    }
}
