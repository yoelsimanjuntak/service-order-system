<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 30/01/2020
 * Time: 11:03
 */
$data = array();
$i = 0;
$user = GetLoggedUser();
foreach ($res as $d) {
  if($user[COL_ROLEID]==ROLEADMIN) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_ID_ORDER] . '" />',
        anchor('order/edit/'.$d[COL_ID_ORDER], str_pad($d[COL_ID_ORDER], 5, '0', STR_PAD_LEFT)),
        date("Y-m-d", strtotime($d[COL_DATE])),
        $d[COL_NM_SERVICE],
        anchor('order/status-edit/'.$d[COL_ID_ORDER],$d[COL_NM_STATUS],array('class' => 'modal-popup-status', 'data-status' => $d[COL_ID_STATUS], 'data-color' => $d[COL_NM_LABELCOLOR])),
        //'<span class="badge" style="background-color: '.$d[COL_NM_LABELCOLOR].'; color: #fff">'.strtoupper($d[COL_NM_STATUS]).'</span>',
        anchor('order/mechanic-edit/'.$d[COL_ID_ORDER],$d[COL_NM_MECHANIC]?$d[COL_NM_MECHANIC]:'(belum diatur)',array('class' => 'modal-popup-mechanic', 'data-mechanic' => $d[COL_ID_MECHANIC])),
        $d[COL_NM_CUSTOMER],
        $d[COL_NM_TYPE],
        $d[COL_NO_PLAT],
        //$d[COL_CREATEDBY],
        //date("Y-m-d H:i:s", strtotime($d[COL_CREATEDON]))
    );
  } else {
    $res[$i] = array(
      $i+1,
      str_pad($d[COL_ID_ORDER], 5, '0', STR_PAD_LEFT),
      date("Y-m-d", strtotime($d[COL_DATE])),
      '<span class="badge" style="background-color: '.$d[COL_NM_LABELCOLOR].'; color: #fff">'.strtoupper($d[COL_NM_STATUS]).'</span>',
      $d[COL_NM_MECHANIC],
      $d[COL_NM_CUSTOMER],
      $d[COL_NM_TYPE],
      $d[COL_NO_PLAT],
    );
  }
  $i++;
}
$data = json_encode($res);
?>

<?php
if($user[COL_ROLEID] == ROLEADMIN) $this->load->view('header');
else $this->load->view('header-front');
?>
    <div class="content-header">
        <div class="<?=$user[COL_ROLEID] == ROLEADMIN?'container-fluid':'container'?>">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3 class="m-0 font-weight-light"><?= $title ?></h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="<?=$user[COL_ROLEID] == ROLEADMIN?'container-fluid':'container'?>">
            <div class="row">
                <div class="col-sm-12">
                    <p>
                      <?php
                      if($user[COL_ROLEID] == ROLEADMIN) {
                        ?>
                        <?=anchor('order/delete','<i class="fa fa-trash-o"></i> HAPUS',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                        <?php
                      }
                      ?>
                      <?=anchor('order/add','<i class="fa fa-plus"></i> TAMBAH',array('class'=>'btn btn-primary btn-sm'))?>
                    </p>
                    <div class="card card-default">
                      <div class="card-header">
                        <?=form_open_multipart(current_url(), array('method'=>'get', 'role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div class="row">
                          <div class="col-sm-3">
                            <div class="form-group row">
                              <label class="control-label col-sm-12">Tanggal</label>
                              <div class="col-sm-5">
                                <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" name="DateFrom" value="<?=$filter['DateFrom']?>" required />
                              </div>
                              <label class="control-label col-sm-2 text-center">s.d</label>
                              <div class="col-sm-5">
                                <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" name="DateTo" value="<?=$filter['DateTo']?>" required />
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group row">
                              <label class="control-label col-sm-12">Status</label>
                              <div class="col-sm-12">
                                <select name="<?=COL_ID_STATUS?>" class="form-control">
                                    <?=GetCombobox("SELECT * FROM mstatus ORDER BY ID_Status", COL_ID_STATUS, COL_NM_STATUS,$filter[COL_ID_STATUS],true,false,'-- Semua --')?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group row">
                              <label class="control-label col-sm-12">No. Registrasi</label>
                              <div class="col-sm-12">
                                <input type="text" class="form-control" name="<?=COL_ID_ORDER?>" value="<?=$filter[COL_ID_ORDER]?>" placeholder="Nomor Registrasi" />
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group row">
                              <label class="control-label col-sm-12">Keyword</label>
                              <div class="col-sm-12">
                                <input type="text" class="form-control" name="Keyword" value="<?=$filter['Keyword']?>" placeholder="Kata Kunci" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 text-right">
                            <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i>&nbsp;RESET</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-filter"></i>&nbsp;FILTER</button>
                          </div>
                        </div>

                        <?=form_close()?>
                      </div>
                        <div class="card-body">
                            <form id="dataform" method="post" action="#">
                                <table id="datalist" class="table table-bordered table-hover">

                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="modal-status" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Ubah Status</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-times fa-sm"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <p class="text-danger error-message"></p>
            <form id="form-editor-status" method="post" action="#">
              <div class="form-group row">
                <label class="control-label col-sm-4">Status</label>
                <div class="col-sm-8">
                  <select name="<?=COL_ID_STATUS?>" class="form-control" required>
                      <?=GetCombobox("SELECT * FROM mstatus ORDER BY ID_Status", COL_ID_STATUS, COL_NM_STATUS)?>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cancel</button>
            <button type="button" class="btn btn-primary btn-ok"><i class="fa fa-pencil"></i>&nbsp;Ubah</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modal-mechanic" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Tugaskan Mekanik</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fa fa-times fa-sm"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <p class="text-danger error-message"></p>
            <form id="form-editor-mechanic" method="post" action="#">
              <div class="form-group row">
                <label class="control-label col-sm-4">Mekanik</label>
                <div class="col-sm-8">
                  <select name="<?=COL_ID_MECHANIC?>" class="form-control" required>
                      <?=GetCombobox("SELECT * FROM mmechanic ORDER BY Nm_Mechanic", COL_ID_MECHANIC, COL_NM_MECHANIC, null, true, false, 'Belum Diatur')?>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cancel</button>
            <button type="submit" class="btn btn-primary btn-ok"><i class="fa fa-pencil"></i>&nbsp;Ubah</button>
          </div>
        </div>
      </div>
    </div>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
              "autoWidth":false,
              //"sDom": "Rlfrtip",
              "aaData": <?=$data?>,
              //"bJQueryUI": true,
              //"aaSorting" : [[5,'desc']],
              "scrollY" : '44vh',
              "scrollX": "120%",
              "iDisplayLength": 100,
              "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
              //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
              "dom":"R<'row'<'col-sm-12'l>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
              "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
              "order": [[ 2, "desc" ]],
              'columnDefs': [
                  {"targets": 2,"className": "text-right"},
                  {"targets": [1,2,3,6],"className": "nowrap"}
              ],
              "aoColumns": [
                  <?=$user[COL_ROLEID]==ROLEADMIN?'{"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false}':'{"sTitle": "#", "width": "10px","bSortable":false}'?> ,
                  {"sTitle": "No. Registrasi"},
                  {"sTitle": "Tanggal"},
                  {"sTitle": "Jenis Service"},
                  {"sTitle": "Status"},
                  {"sTitle": "Mekanik"},
                  {"sTitle": "Konsumen"},
                  {"sTitle": "Kendaraan"},
                  {"sTitle": "No. Plat"},
                  //{"sTitle": "Created By"},
                  //{"sTitle": "Created On"}
              ],
              "createdRow": function(row, data, dataIndex) {
                //console.log(row);
                var color = $(data[4]).data('color');
                $(row).css("background-color", color);
              }
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });

            $("#modal-status").on("shown.bs.modal", function(){
              $("select", $("#modal-status")).select2({ width: 'resolve', theme: 'bootstrap4' });
            });
            $("#modal-mechanic").on("shown.bs.modal", function(){
              $("select", $("#modal-mechanic")).select2({ width: 'resolve', theme: 'bootstrap4' });
            });
            $('.modal-popup-status').click(function(){
                var a = $(this);
                var status = $(this).data('status');
                var editor = $("#modal-status");

                $('[name=<?=COL_ID_STATUS?>]', editor).val(status);
                editor.modal("show");
                $(".btn-ok", editor).unbind('click').click(function() {
                    $(this).html("Loading...").attr("disabled", true);
                    $('#form-editor-status').ajaxSubmit({
                        dataType: 'json',
                        url : a.attr('href'),
                        success : function(data){
                            if(data.error==0){
                                window.location.reload();
                            }else{
                                toastr.error(data.error);
                            }
                        },
                        error: function() {
                          toastr.error('Server Problem');
                        },
                        complete: function() {
                          $(this).html('<i class="fa fa-pencil"></i>&nbsp;Ubah').attr("disabled", false);
                          editor.modal("hide");
                        }
                    });
                });
                return false;
            });
            $('.modal-popup-mechanic').click(function(){
                var a = $(this);
                var mechanic = $(this).data('mechanic');
                var editor = $("#modal-mechanic");

                $('[name=<?=COL_ID_MECHANIC?>]', editor).val(mechanic);
                editor.modal("show");
                $(".btn-ok", editor).unbind('click').click(function() {
                    $(this).html("Loading...").attr("disabled", true);
                    $('#form-editor-mechanic').ajaxSubmit({
                        dataType: 'json',
                        url : a.attr('href'),
                        success : function(data){
                            if(data.error==0){
                                window.location.reload();
                            }else{
                                toastr.error(data.error);
                            }
                        },
                        error: function() {
                          toastr.error('Server Problem');
                        },
                        complete: function() {
                          $(this).html('<i class="fa fa-pencil"></i>&nbsp;Ubah').attr("disabled", false);
                          editor.modal("hide");
                        }
                    });
                });
                return false;
            });

            $('button[type=reset]').click(function() {
              $('input[type=text],select', $('#main-form')).not('[name=DateFrom], [name=DateTo]').val('').trigger('change');
              return false;
            });
        });
    </script>

<?php
if($user[COL_ROLEID] == ROLEADMIN) $this->load->view('footer');
else $this->load->view('footer-front');
?>
