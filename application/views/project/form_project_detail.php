<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/3/2019
 * Time: 10:44 AM
 */
$this->load->view('header');
?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small> Detail</small></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=site_url('project/index')?>"> <?=$title?></a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title"><?=$data[COL_NM_PROJECT]?></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th style="width: 200px">Category</th>
                                <td style="width: 10px">:</td>
                                <td><?=$data[COL_NM_CATEGORY]?></td>

                                <th style="width: 200px">PM</th>
                                <td style="width: 10px">:</td>
                                <td><?=$data["NM_PM"]?></td>
                            </tr>
                            <tr>
                                <th style="width: 200px">Customer / Owner</th>
                                <td style="width: 10px">:</td>
                                <td><?=$data[COL_NM_CUSTOMER]?></td>

                                <th style="width: 200px">PMO</th>
                                <td style="width: 10px">:</td>
                                <td><?=$data["NM_PMO"]?></td>
                            </tr>
                            <tr>
                                <th style="width: 200px">Status / Mandays</th>
                                <td style="width: 10px">:</td>
                                <td>
                                    <?=!empty($data[COL_NM_LABELCOLOR])?'<span class="badge" style="color: #fff;background-color: '.$data[COL_NM_LABELCOLOR].'">'.strtoupper($data[COL_NM_STATUS]).'</span>':$data[COL_NM_STATUS]?>
                                    &nbsp;/&nbsp;
                                    <span class="badge bg-primary"><?=$data[COL_MANDAYS]?></span>
                                </td>

                                <th style="width: 200px">Attachment</th>
                                <td style="width: 10px">:</td>
                                <td>
                                    <?php
                                    if(!empty($data) && !empty($data[COL_NM_ATTACHMENT])) {
                                        ?>
                                        <a href="<?=MY_UPLOADURL.$data[COL_NM_ATTACHMENT]?>" target="_blank">
                                            <i class="fa fa-paperclip"></i>&nbsp;&nbsp;<?=$data[COL_NM_ATTACHMENT]?>
                                        </a>
                                    <?php
                                    } else {
                                        echo "-";
                                    }
                                    ?>

                                </td>
                            </tr>
                            <tr>
                                <th style="width: 200px">Description</th>
                                <td style="width: 10px">:</td>
                                <td colspan="4">
                                    <p style="text-align: justify"><?=$data[COL_NM_PROJECTDESC]?></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="card-tasks" class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">Project Tasks</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?=site_url('project/tasks/'.$data[COL_ID_PROJECT])?>"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
                <div id="card-tasks" class="card card-outline card-success">
                    <div class="card-header">
                        <h3 class="card-title">Comments</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="disqus_thread"></div>
                        <script>

                            /**
                             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                            /*
                             */
                            var disqus_config = function () {
                                this.page.url = '<?=site_url('project/detail/'.$data[COL_ID_PROJECT])?>';  // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier = '<?=site_url('project/detail/'.$data[COL_ID_PROJECT])?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };
                            (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document, s = d.createElement('script');
                                s.src = '<?=$this->setting_web_disqus_url?>';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $("[data-card-widget=card-refresh]", $("#card-tasks")).click();
            $("#modal-task").on('hidden.bs.modal', function(){
                $(this).find(".modal-body").html("Loading...");
            });
        });
    </script>
    <script id="dsq-count-scr" src="//project-tracker.disqus.com/count.js" async></script>
<?php $this->load->view('footer') ?>