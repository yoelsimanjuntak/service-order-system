<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/2/2019
 * Time: 1:15 AM
 */

$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=site_url('project/index')?>"> <?=$title?></a></li>
                        <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-default">
                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'userForm','class'=>'form-horizontal'))?>
                        <div class="card-header">
                            <h3 class="card-title">Project Information</h3>
                        </div>
                        <div class="card-body">
                            <div style="display: none" class="alert alert-danger errorBox">
                                <i class="fa fa-ban"></i> Error :
                                <span class="errorMsg"></span>
                            </div>
                            <?php
                            if($this->input->get('error') == 1){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <span class="">Data gagal disimpan, silahkan coba kembali</span>
                                </div>
                            <?php
                            }
                            if(validation_errors()){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=validation_errors()?>
                                </div>
                            <?php
                            }
                            if(!empty($upload_errors)) {
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=$upload_errors?>
                                </div>
                            <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Category</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_CATEGORY?>" class="form-control" required>
                                                <option value="">-- Select Category --</option>
                                                <?=GetCombobox("SELECT * FROM mcategory ORDER BY ID_Category", COL_ID_CATEGORY, COL_NM_CATEGORY, (!empty($data[COL_ID_CATEGORY]) ? $data[COL_ID_CATEGORY] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Customer / Owner</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_CUSTOMER?>" class="form-control" required>
                                                <option value="">-- Select Customer / Owner --</option>
                                                <?=GetCombobox("SELECT * FROM mcustomer ORDER BY NM_Customer", COL_ID_CUSTOMER, COL_NM_CUSTOMER, (!empty($data[COL_ID_CUSTOMER]) ? $data[COL_ID_CUSTOMER] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Status</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_STATUS?>" class="form-control" required>
                                                <option value="">-- Select Status --</option>
                                                <?=GetCombobox("SELECT * FROM mstatus where IS_Project = 1 ORDER BY NM_Status", COL_ID_STATUS, COL_NM_STATUS, (!empty($data[COL_ID_STATUS]) ? $data[COL_ID_STATUS] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">PM</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_PM?>" class="form-control" required <?=$ruser[COL_ROLEID] == ROLE_PM ? "disabled" : ""?>>
                                                <option value="">-- Select Project Manager --</option>
                                                <?=GetCombobox("SELECT * FROM memployee left join userinformation u on u.CompanyID = memployee.ID_Employee left join users us on us.UserName = u.UserName where us.RoleID = ".ROLE_PM." ORDER BY NM_Employee", COL_ID_EMPLOYEE, array(COL_NM_NIK, COL_NM_EMPLOYEE), (!empty($data[COL_ID_PM]) ? $data[COL_ID_PM] : ($ruser[COL_ROLEID]==ROLE_PM?$ruser[COL_COMPANYID]:"")))?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">PMO</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ID_PMO?>" class="form-control" required>
                                                <option value="">-- Select Project Manager Officer --</option>
                                                <?=GetCombobox("SELECT * FROM memployee left join userinformation u on u.CompanyID = memployee.ID_Employee left join users us on us.UserName = u.UserName where us.RoleID = ".ROLE_PMO." ORDER BY NM_Employee", COL_ID_EMPLOYEE, array(COL_NM_NIK, COL_NM_EMPLOYEE), (!empty($data[COL_ID_PMO]) ? $data[COL_ID_PMO] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Project Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="<?=COL_NM_PROJECT?>" value="<?= $edit ? $data[COL_NM_PROJECT] : ""?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Project Mandays</label>
                                        <div class="col-sm-3">
                                            <input type="number" class="form-control" name="<?=COL_MANDAYS?>" value="<?= $edit ? $data[COL_MANDAYS] : ""?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Project Description</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" rows="5" name="<?=COL_NM_PROJECTDESC?>" required="true"><?= $edit ? $data[COL_NM_PROJECTDESC] : ""?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Attachment</label>
                                        <div class="col-sm-8">
                                            <?php
                                            if(!empty($data) && !empty($data[COL_NM_ATTACHMENT])) {
                                                ?>
                                                <p>
                                                    <a href="<?=MY_UPLOADURL.$data[COL_NM_ATTACHMENT]?>" target="_blank" class="btn btn-default btn-block btn-flat">
                                                        <i class="fa fa-paperclip"></i>&nbsp;&nbsp;<?=$data[COL_NM_ATTACHMENT]?>
                                                    </a>
                                                </p>
                                            <?php
                                            }
                                            ?>
                                            <input type="file" name="userfile" />
                                            <!--<div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" name="userfile" class="custom-file-input">
                                                    <label class="custom-file-label" for="userfile">Choose file</label>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?=site_url('project/index')?>" class="btn btn-default"><i class="fa fa-angle-left"></i> BACK</a>
                                    <?php
                                    if($edit) {
                                        ?>
                                        <a href="<?=site_url('project/detail/'.$data[COL_ID_PROJECT])?>" class="btn btn-success"><i class="fa fa-info-circle"></i> DETAIL</a>
                                        <?php
                                    }
                                    ?>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> SUBMIT</button>
                                </div>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
<?php $this->load->view('footer') ?>