<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/3/2019
 * Time: 1:51 PM
 */
$ruser = GetLoggedUser();
?>
<?php
if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLE_PM || $ruser[COL_ROLEID] == ROLE_PMO) {
    ?>
    <p>
        <a href="<?=site_url('task/delete')?>" class="btn btn-xs btn-danger cekboxaction" data-confirm="Are you sure to delete?"><i class="fas fa-trash"></i> REMOVE</a>
        <button type="button" class="btn btn-xs btn-primary" id="btn-add-task"><i class="fas fa-pencil-square-o"></i> CREATE NEW</button>
    </p>
<?php
}
?>
<form id="dataform" method="post" action="#">
    <table class="table table-bordered" style="font-size: 11pt">
        <thead>
        <tr>
            <?php
            if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLE_PM || $ruser[COL_ROLEID] == ROLE_PMO) {
                ?>
                <th style="width: 10px">
                    <input type="checkbox" id="cekbox" />
                </th>
                <?php
            }
            ?>
            <th>Task</th>
            <th>Priority</th>
            <th>Status</th>
            <th>Start (Est.)</th>
            <th>End (Est.)</th>
            <th>Mandays</th>
            <th style="text-align: center"><i class="fa fa-users"></i> </th>
            <th style="text-align: center; width: 10px"><i class="fa fa-paperclip"></i> </th>
        </tr>
        </thead>
        <tbody>
        <?php
        $tasks = $this->db
            ->select("tproject_task.*, mpriority.NM_Priority, mstatus.NM_Status, mstatus.NM_LabelColor, (select count(*) from tproject_employee where ID_Task = tproject_task.ID_Task) as Emps")
            ->join(TBL_MPRIORITY,TBL_MPRIORITY.'.'.COL_ID_PRIORITY." = ".TBL_TPROJECT_TASK.".".COL_ID_PRIORITY,"left")
            ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_ID_STATUS." = ".TBL_TPROJECT_TASK.".".COL_ID_STATUS,"left")
            ->where(COL_ID_PROJECT, $data[COL_ID_PROJECT])
            ->order_by(TBL_TPROJECT_TASK.".".COL_NM_PLANNEDSTART)
            ->order_by(TBL_MPRIORITY.".".COL_NM_SORTVALUE)
            ->get(TBL_TPROJECT_TASK)
            ->result_array();
        foreach($tasks as $t) {
            ?>
            <tr>
                <?php
                if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLE_PM || $ruser[COL_ROLEID] == ROLE_PMO) {
                    ?>
                    <td><input type="checkbox" class="cekbox" name="cekbox[]" value="<?=$t[COL_ID_TASK]?>" /></td>
                <?php
                }
                ?>
                <td>
                    <?php
                    if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLE_PM || $ruser[COL_ROLEID] == ROLE_PMO) {
                        ?>
                        <a href="javascript: void(0)" onclick="getDetailTask('<?=site_url("task/edit/".$t[COL_ID_TASK]."/1")?>')"><?=$t[COL_NM_TASK]?></a>
                        <?php
                    } else {
                        echo anchor('task/detail/'.$t[COL_ID_TASK],$t[COL_NM_TASK], array('target'=>'_blank'));
                    }
                    ?>

                </td>
                <td><?=$t[COL_NM_PRIORITY]?></td>
                <td><?=!empty($t[COL_NM_LABELCOLOR])?'<span class="badge" style="color: #fff;background-color: '.$t[COL_NM_LABELCOLOR].'">'.strtoupper($t[COL_NM_STATUS]).'</span>':$t[COL_NM_STATUS]?></td>
                <td><?=date('d-m-Y', strtotime($t[COL_NM_PLANNEDSTART]))?></td>
                <td><?=date('d-m-Y', strtotime($t[COL_NM_PLANNEDEND]))?></td>
                <td style="text-align: right"><?=number_format($t[COL_MANDAYS_CONSUMED])?></td>
                <td style="text-align: right"><?=number_format($t["Emps"])?></td>
                <td>
                    <?php
                    if(!empty($t[COL_NM_ATTACHMENT])) {
                        ?>
                        <a href="<?=MY_UPLOADURL.$t[COL_NM_ATTACHMENT]?>" target="_blank">
                            <i class="fa fa-download"></i>
                        </a>
                    <?php
                    }
                    ?>
                </td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</form>
<div class="modal fade" id="modal-task">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Task</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Loading ...</p>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('loadjs') ?>
<script>
    function refreshTask() {
        $("[data-card-widget=card-refresh]", $("#card-tasks")).click();
    }
    function getDetailTask(url) {
        var modal = $("#modal-task");
        modal.modal('show');

        $(".modal-body", modal).load(url, function() {
            $("select", modal).not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
            $('.datepicker', modal).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1970,
                maxYear: parseInt(moment().format('YYYY'),10),
                locale: {
                    format: 'Y-MM-DD'
                }
            });
            $("#task-form", modal).validate({
                submitHandler : function(form){
                    modal.find(".modal-body").append('<div class="overlay"><i class="fas fa-2x fa-sync-alt"></i></div>');
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        type : 'post',
                        success : function(data){
                            if(data.error != 0){
                                toastr.error(data.error);
                            }else{
                                toastr.success('Success updating task.');
                                modal.modal("hide");
                            }
                        },
                        error : function(a,b,c){
                            toastr.error(a.responseText);
                            modal.modal("hide");
                        },
                        complete : function() {
                            modal.find(".overlay").remove();
                            refreshTask();
                        }
                    });
                    return false;
                }
            });
        });
    }
    $(document).ready(function() {
        $("#btn-add-task").click(function() {
            var modal = $("#modal-task");
            modal.modal('show');

            $(".modal-body", modal).load('<?=site_url("task/add/".$data[COL_ID_PROJECT]."/1")?>', function() {
                $("select", modal).not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
                $('.datepicker', modal).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1970,
                    maxYear: parseInt(moment().format('YYYY'),10),
                    locale: {
                        format: 'Y-MM-DD'
                    }
                });
                $("#task-form", modal).validate({
                    submitHandler : function(form){
                        modal.find(".modal-body").append('<div class="overlay"><i class="fas fa-2x fa-sync-alt"></i></div>');
                        $(form).ajaxSubmit({
                            dataType: 'json',
                            type : 'post',
                            success : function(data){
                                if(data.error != 0){
                                    toastr.error(data.error);
                                }else{
                                    toastr.success('Success addding task.');
                                    modal.modal("hide");
                                }
                            },
                            error : function(a,b,c){
                                toastr.error(a.responseText);
                                modal.modal("hide");
                            },
                            complete : function() {
                                modal.find(".overlay").remove();
                                refreshTask();
                            }
                        });
                        return false;
                    }
                });
            });
        });
        $('#cekbox').click(function(){
            if($(this).is(':checked')){
                $('.cekbox').prop('checked',true);
            }else{
                $('.cekbox').prop('checked',false);
            }
        });
    });
</script>
