<?php $this->load->view('header-front') ?>
<style>
.timeline::before {
  display: none;
}
</style>
<div class="content-header">
    <div class="container">
        <!--<div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
        </div>-->
    </div>
</div>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div id="card-service" class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Daftar Antrian <small class="font-italic"><?=HariIni().", ".date('d-m-Y')?></small></h5>
                        <div class="card-tools">
                            <!--<div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="table_search" class="form-control float-right" placeholder="Cari">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    <button type="button" class="btn btn-default" data-card-widget="card-refresh" data-source="<?=site_url('order/list-today')?>"><i class="fas fa-sync-alt"></i></button>
                                </div>
                            </div>-->
                            <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?=site_url('order/index-order-today/'.date('Y-m-d'))?>"><i class="fas fa-sync-alt"></i></button>
                        </div>
                    </div>
                    <div class="card-body p-0">

                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if(!IsLogin()) {
                                    ?>
                                    <p class="text-right">Ingin masuk ke antrian? Silakan <?=anchor('user/register/','daftar')?> terlebih dahulu.</p>
                                    <?php
                                } else {
                                    ?>
                                    <p>
                                        <a href="<?=site_url('order/add')?>" class="btn btn-danger"><i class="fa fa-plus"></i>&nbsp;Tambah Antrian</a>
                                    </p>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title m-0">Berita</h5>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <?php
                        if(!empty($berita)) {
                          ?>
                          <div class="timeline timeline-inverse mb-0 w-100">
                            <?php
                            foreach($berita as $n) {
                              ?>
                              <div class="row m-0 mb-2">
                                <div class="timeline-item m-0 w-100">
                                  <span class="time"><i class="fa fa-calendar"></i> <?=date('d-m-Y', strtotime($n[COL_POSTDATE]))?></span>
                                  <h3 class="timeline-header font-weight-bold"><a href="<?=site_url('home/page/'.$n[COL_POSTSLUG])?>"><?=$n[COL_POSTTITLE]?></a></h3>
                                  <div class="timeline-body">
                                    <?php
                                    $rimages = $this->db->where(COL_POSTID, $n[COL_POSTID])->limit(4)->get(TBL_POSTIMAGES)->result_array();
                                    if(!empty($rimages)) {
                                      ?>
                                      <div class="row">
                                        <div class="col-sm-12 text-center">
                                          <?php
                                          foreach ($rimages as $img) {
                                            ?>
                                            <a href="<?=MY_UPLOADURL.$img[COL_FILENAME]?>" data-toggle="lightbox" data-title="<?=$n[COL_POSTTITLE]?>" data-gallery="gallery">
                                              <img src="<?=MY_UPLOADURL.$img[COL_FILENAME]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                            </a>
                                            <?php
                                          }
                                          ?>
                                        </div>
                                      </div>
                                      <br  />
                                      <?php
                                    }
                                    ?>
                                    <?php
                                    $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                                    ?>
                                    <?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?>
                                  </div>
                                </div>
                              </div>
                              <?php
                            }
                             ?>
                          </div>
                          <?php
                        } else {
                          ?>
                          <p class="font-italic">
                            Tidak ada data untuk ditampilkan.
                          </p>
                          <?php
                        }
                         ?>
                      </div>
                    </div>
                    <div class="card-footer text-right">
                      <a href="<?=site_url('home/post/1')?>" class="btn btn-danger"><i class="fa fa-list"></i> Selengkapnya (<strong><?=$count_berita?></strong>)</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('loadjs') ?>
<script>
    $(document).ready(function() {
        $('.btn-tool', $("#card-service")).click();
    });
</script>
<?php $this->load->view('footer-front') ?>
