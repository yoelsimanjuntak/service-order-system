<?php $this->load->view('header-front') ?>
<section class="content-wrapper pt-4">
  <section class="content-header">
    <div class="container">
      <div class="row mb-2">
          <div class="col-sm-6">
              <!--<h1 class="m-0 text-dark"><?= $title ?></h1>-->
          </div>
          <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
                  <li class="breadcrumb-item active"><?=$title?></li>
              </ol>
          </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card card-outline card-danger">
            <div class="card-header text-center">
              <h2 class="card-title float-none"><?= $data[COL_POSTTITLE] ?></h2>
            </div>
            <div class="card-body">
              <?php
              $files = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
              if(!empty($files)) {
                ?>
                <div class="row d-flex align-items-stretch mb-2">
                  <?php
                  foreach($files as $f) {
                    if(strpos(mime_content_type(MY_UPLOADPATH.$f[COL_FILENAME]), 'image') !== false) {
                      ?>
                      <div class="col-12 col-sm-12 col-md-4 d-flex align-items-stretch">
                        <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" data-toggle="lightbox" data-title="<?=$data[COL_POSTTITLE]?>" data-gallery="gallery">
                          <img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" class="img-fluid mb-2" alt="<?=$data[COL_POSTTITLE]?>">
                        </a>
                      </div>
                      <?php
                    } else {
                      ?>
                      <div class="col-12 col-sm-12 col-md-12 mb-3 d-flex align-items-stretch">
                        <embed src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" width="100%" height="600" />
                      </div>
                      <?php
                    }
                    ?>
                  <?php
                  }
                  ?>
                </div>
                <?php
              }
              ?>
              <?=$data[COL_POSTCONTENT]?>
              <div class="row bt-1">
                <p class="font-sm font-weight-light">
                  <i class="fa fa-user"></i>&nbsp;&nbsp;<?=$data[COL_NAME]?><br  />
                  <i class="fa fa-calendar"></i>&nbsp;&nbsp;<?=date('d-m-Y, H:i:s', strtotime($data[COL_CREATEDON]))?>
                </p>
              </div>
            </div>
            <div class="card-footer">
                <div id="disqus_thread"></div>
                <script>

                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                    /*
                     */
                    var disqus_config = function () {
                        this.page.url = '<?=site_url('home/page/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };
                    (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = '<?=$this->setting_web_disqus_url?>';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<?php $this->load->view('loadjs') ?>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script>
$(document).ready(function() {
  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({
      alwaysShowClose: true
    });
  });
});
</script>
<?php $this->load->view('footer-front') ?>
