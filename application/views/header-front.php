
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

    <style>
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
        }
        #footer-section::after {
          background: rgba(0, 0, 0, 0) url(<?=MY_IMAGEURL?>footer-map-bg.png) no-repeat scroll center center / 75% auto;
          content: "";
          height: 100%;
          left: 0;
          opacity: 0.1;
          position: absolute;
          top: 0;
          width: 100%;
          z-index: -1;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('.btn-login', $('.login-section')).click(function() {
                var form = $('#login-form');
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            toastr.error(data.error);
                        }else{
                            location.reload();
                        }
                    },
                    error : function(a,b,c){
                        toastr.error('Response Error');
                    }
                });
            });
            $(".se-pre-con").fadeOut("slow");
        });
    </script>
</head>
<body class="hold-transition layout-top-nav">
<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_IMAGEFILENAME] ? MY_UPLOADURL.$ruser[COL_IMAGEFILENAME] : MY_IMAGEURL.'user.jpg';
}
?>
<div class="se-pre-con"></div>
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
        <div class="container">
            <a href="<?=site_url()?>" class="navbar-brand">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image">
                <span class="brand-text font-weight-light"><?=$this->setting_web_name?></span>
            </a>
            <ul class="navbar-nav ml-auto">
                <!--<li class="nav-item dropdown ">
                    <a class="nav-link" href="<?=site_url()?>" title="Beranda">
                        <i class="fa fa-home"></i>
                    </a>
                </li>-->
                <?php
                if(IsLogin()) {
                    ?>
                    <?php
                    if($ruser[COL_ROLEID] != ROLEADMIN) {
                      ?>
                      <li class="nav-item d-none d-sm-inline-block">
                          <a href="<?=site_url('order/index')?>" class="nav-link">Antrian Saya</a>
                      </li>
                      <?php
                    }
                    ?>
                    <li class="nav-item dropdown user-menu">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <img src="<?=$displaypicture?>" class="user-image img-circle elevation-1" alt="<?=$displayname?>">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <li class="user-header bg-danger">
                                <img src="<?=$displaypicture?>" class="img-circle elevation-2" alt="<?=$displayname?>">

                                <p>
                                    <?=$ruser[COL_NAME]?>
                                    <small><?=strtoupper($ruser[COL_ROLENAME])?></small>
                                </p>
                            </li>
                            <li class="user-footer">
                              <?php
                              if($ruser[COL_ROLEID]==ROLEADMIN) {
                                ?>
                                <a href="<?=site_url('user/dashboard')?>" class="btn btn-outline-danger"><i class="fa fa-dashboard"></i> Dashboard</a>
                                <?php
                              } else {
                                ?>
                                <!--<a href="<?=site_url('user/profile')?>" class="btn btn-primary"><i class="fa fa-user"></i> Profil</a>-->
                                <?php
                              }
                               ?>
                                <a href="<?=site_url('user/logout')?>" class="btn btn-default float-right">Logout</a>
                            </li>
                        </ul>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="nav-item dropdown user-menu">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" title="Login">
                            <i class="fa fa-sign-in"></i>&nbsp;Login
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right login-section">
                            <li class="user-header bg-default">
                                <?= form_open(site_url('user/login-guest'),array('id'=>'login-form')) ?>
                                <p class="text-left" style="margin-bottom: 5px; text-decoration: underline">Login</p>
                                <p class="text-left" style="margin-bottom: 10px; margin-top: 5px;">
                                    <small>Belum punya akun? Silakan <a href="<?=site_url('user/register')?>">daftar</a>.</small>
                                </p>
                                <div>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-sm" name="<?=COL_USERNAME?>" placeholder="Username" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="password" class="form-control form-control-sm" name="<?=COL_PASSWORD?>" placeholder="Password" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <span class="fas fa-key"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </li>
                            <li class="user-footer">
                                <button type="button" class="btn btn-danger btn-login"><i class="fa fa-sign-in"></i>&nbsp;&nbsp;Login</button>
                            </li>
                        </ul>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </nav>
    <div class="content-wrapper">
