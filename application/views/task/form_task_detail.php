<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/8/2019
 * Time: 1:29 PM
 */
$this->load->view('header');
$ruser = GetLoggedUser();
$emps = [];
if(!empty($data)) {
    $emps = $this->db->where(array(COL_ID_TASK=>$data[COL_ID_TASK], COL_ID_EMPLOYEE=>$ruser[COL_COMPANYID]))->get(TBL_TPROJECT_EMPLOYEE)->result_array();
}
?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small> Detail</small></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=site_url('task/index')?>"> <?=$title?></a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title"><?=$data[COL_NM_PROJECT]?></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <?=form_open(site_url('task/change-status/'.$data[COL_ID_TASK]),array('role'=>'form','id'=>'task-form','class'=>'form-horizontal'))?>
                    <input type="hidden" name="<?=COL_ID_PROJECT?>" value="<?=$data[COL_ID_PROJECT]?>" />
                    <div class="card-body p-0">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th style="width: 200px">Status</th>
                                <td style="width: 10px">:</td>
                                <td>
                                    <select name="<?=COL_ID_STATUS?>" class="form-control" required <?=$ruser[COL_ROLEID]==ROLE_EMPLOYEE&&count($emps)<=0?'disabled':''?>>
                                        <option value="">-- Select Status --</option>
                                        <?=GetCombobox("SELECT * FROM mstatus ORDER BY NM_Status", COL_ID_STATUS, COL_NM_STATUS, (!empty($data[COL_ID_STATUS]) ? $data[COL_ID_STATUS] : null))?>
                                    </select>
                                </td>

                                <th style="width: 200px">Priority</th>
                                <td style="width: 10px">:</td>
                                <td><?=$data[COL_NM_PRIORITY]?></td>
                            </tr>
                            <tr>
                                <th style="width: 200px">Assignment</th>
                                <td style="width: 10px">:</td>
                                <td>
                                    <?php
                                    $emps = $this->db
                                        ->join(TBL_MEMPLOYEE,TBL_MEMPLOYEE.'.'.COL_ID_EMPLOYEE." = ".TBL_TPROJECT_EMPLOYEE.".".COL_ID_EMPLOYEE,"left")
                                        ->where(COL_ID_TASK, $data[COL_ID_TASK])
                                        ->get(TBL_TPROJECT_EMPLOYEE)
                                        ->result_array();
                                    if(count($emps) > 0) {
                                        if(count($emps) > 1) {
                                            $html = "<ol style='padding-left: 12px'>";
                                            foreach($emps as $e) {
                                                $html .= "<li>".$e[COL_NM_EMPLOYEE]."</li>";
                                            }
                                            $html .= "</ol>";
                                            echo number_format(count($emps)); //$html;
                                        } else {
                                            echo $emps[0][COL_NM_EMPLOYEE];
                                        }
                                    }
                                    ?>
                                </td>

                                <th style="width: 200px">Attachment</th>
                                <td style="width: 10px">:</td>
                                <td>
                                    <?php
                                    if(!empty($data) && !empty($data[COL_NM_ATTACHMENT])) {
                                        ?>
                                        <a href="<?=MY_UPLOADURL.$data[COL_NM_ATTACHMENT]?>" target="_blank">
                                            <i class="fa fa-paperclip"></i>&nbsp;&nbsp;<?=$data[COL_NM_ATTACHMENT]?>
                                        </a>
                                    <?php
                                    } else {
                                        echo "-";
                                    }
                                    ?>

                                </td>
                            </tr>
                            <tr>
                                <th style="width: 200px">Description</th>
                                <td style="width: 10px">:</td>
                                <td colspan="4">
                                    <p style="text-align: justify"><?=$data[COL_NM_TASK]?></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer" style="text-align: right">
                        <button type="submit" class="btn btn-primary" <?=$ruser!=ROLE_EMPLOYEE?'disabled':''?>><i class="fa fa-save"></i> SUBMIT CHANGES</button>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="card-activity" class="card card-outline card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Activity</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?=site_url('task/activity/'.$data[COL_ID_TASK])?>"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-footer">
                        <?=form_open(site_url('task/activity-add/'.$data[COL_ID_TASK]),array('role'=>'form','id'=>'activity-form','class'=>'form-horizontal'))?>
                        <input type="hidden" name="<?=COL_ID_TASK?>" value="<?=$data[COL_ID_TASK]?>" />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <textarea class="form-control" name="<?=COL_NM_ACTIVITY?>" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="userfile" />
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> SUBMIT</button>
                                </div>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                    <div class="card-body p-0">

                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="card-log" class="card card-outline card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Recent Logs</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?=site_url('task/log/'.$data[COL_ID_TASK])?>"><i class="fas fa-sync-alt"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body pt-0">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $("[data-card-widget=card-refresh]", $("#card-activity")).click();
            $("[data-card-widget=card-refresh]", $("#card-log")).click();

            $("#activity-form").validate({
                submitHandler : function(form){
                    var card = $("#activity-form").closest(".card");
                    card.find(".card-body").append('<div class="overlay"><i class="fas fa-2x fa-sync-alt"></i></div>');
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        type : 'post',
                        success : function(data){
                            if(data.error != 0){
                                toastr.error(data.error);
                            }else{
                                toastr.success('Success adding activity.');
                            }
                        },
                        error : function(a,b,c){
                            toastr.error(a.responseText);
                        },
                        complete : function() {
                            card.find(".overlay").remove();
                            $("[data-card-widget=card-refresh]", $("#card-activity")).click();
                        }
                    });
                    return false;
                }
            });

            $("#task-form").validate({
                submitHandler : function(form){
                    var card = $("#task-form").closest(".card");
                    card.find(".card-body").append('<div class="overlay"><i class="fas fa-2x fa-sync-alt"></i></div>');
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        type : 'post',
                        success : function(data){
                            if(data.error != 0){
                                toastr.error(data.error);
                            }else{
                                toastr.success('Success updating status.');
                            }
                        },
                        error : function(a,b,c){
                            toastr.error(a.responseText);
                        },
                        complete : function() {
                            card.find(".overlay").remove();
                            location.reload();
                        }
                    });
                    return false;
                }
            });
        });
    </script>
<?php $this->load->view('footer') ?>