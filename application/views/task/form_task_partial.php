<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/3/2019
 * Time: 12:43 PM
 */
?>
<?=form_open(current_url(),array('role'=>'form','id'=>'task-form','class'=>'form-horizontal'))?>
    <input type="hidden" name="<?=COL_ID_PROJECT?>" value="<?=!empty($data[COL_ID_PROJECT])?$data[COL_ID_PROJECT]:""?>" />
    <input type="hidden" name="IsAjax" value="<?=!empty($data["IsAjax"])?$data["IsAjax"]:""?>" />
<div class="row">
    <div class="col-sm-7">
        <div class="form-group row">
            <label class="control-label col-sm-3">Priority</label>
            <div class="col-sm-8">
                <select name="<?=COL_ID_PRIORITY?>" class="form-control" required>
                    <option value="">-- Select Priority --</option>
                    <?=GetCombobox("SELECT * FROM mpriority ORDER BY NM_SortValue", COL_ID_PRIORITY, COL_NM_PRIORITY, (!empty($data[COL_ID_PRIORITY]) ? $data[COL_ID_PRIORITY] : null))?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-3">Status</label>
            <div class="col-sm-8">
                <select name="<?=COL_ID_STATUS?>" class="form-control" required>
                    <option value="">-- Select Status --</option>
                    <?=GetCombobox("SELECT * FROM mstatus where IS_Project = 0 ORDER BY ID_Status", COL_ID_STATUS, COL_NM_STATUS, (!empty($data[COL_ID_STATUS]) ? $data[COL_ID_STATUS] : null))?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-3">Title</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="<?=COL_NM_TASK?>" value="<?= $edit ? $data[COL_NM_TASK] : ""?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-3">Assignment</label>
            <div class="col-sm-8">
                <select name="<?=COL_ID_EMPLOYEE?>[]" class="form-control" multiple="multiple" required>
                    <option value="">-- Select Employee --</option>
                    <?=GetCombobox("SELECT * FROM memployee ORDER BY NM_NIK", COL_ID_EMPLOYEE, COL_NM_EMPLOYEE, (!empty($data[COL_ID_EMPLOYEE]) ? explode(",", $data[COL_ID_EMPLOYEE])  : null))?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group row">
            <label class="control-label col-sm-5">Est. Start</label>
            <div class="col-sm-7">
                <input type="text" class="form-control datepicker" name="<?=COL_NM_PLANNEDSTART?>" value="<?= $edit ? $data[COL_NM_PLANNEDSTART] : ""?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-5">Est. End</label>
            <div class="col-sm-7">
                <input type="text" class="form-control datepicker" name="<?=COL_NM_PLANNEDEND?>" value="<?= $edit ? $data[COL_NM_PLANNEDEND] : ""?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-5">Consumed Mandays</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" name="<?=COL_MANDAYS_CONSUMED?>" value="<?= $edit ? $data[COL_MANDAYS_CONSUMED] : ""?>" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-5">Attachment</label>
            <div class="col-sm-7">
                <?php
                if(!empty($data) && !empty($data[COL_NM_ATTACHMENT])) {
                    ?>
                    <p>
                        <a href="<?=MY_UPLOADURL.$data[COL_NM_ATTACHMENT]?>" target="_blank" class="btn btn-default btn-block btn-flat">
                            <i class="fa fa-paperclip"></i>&nbsp;&nbsp;<?=$data[COL_NM_ATTACHMENT]?>
                        </a>
                    </p>
                <?php
                }
                ?>
                <input type="file" name="userfile" />
            </div>
        </div>
    </div>
    <div class="col-md-12 pt-3" style="border-top: 1px solid #dedede; text-align: right">
        <?php
        if(!$this->input->is_ajax_request()) {
            ?>
            <a href="<?=site_url('project/index')?>" class="btn btn-default"><i class="fa fa-angle-left"></i> BACK</a>
        <?php
        }
        ?>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> SUBMIT</button>
    </div>
</div>
<?=form_close()?>