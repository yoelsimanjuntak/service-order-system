<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 29/01/2020
 * Time: 09:43
 */
?>
<?=form_open(current_url(),array('role'=>'form','id'=>'vehicle-form','class'=>'form-horizontal'))?>
    <input type="hidden" name="<?=COL_ID_CUSTOMER?>" value="<?=!empty($data[COL_ID_CUSTOMER])?$data[COL_ID_CUSTOMER]:""?>" />
    <input type="hidden" name="IsAjax" value="<?=!empty($data["IsAjax"])?$data["IsAjax"]:""?>" />
    <div class="row">
        <div class="col-sm-12 pl-3">
            <div class="form-group row">
                <label class="control-label col-sm-3">Tipe Kendaraan</label>
                <div class="col-sm-5">
                    <select name="<?=COL_ID_TYPE?>" class="form-control" required>
                        <option value="">-- Pilih Tipe Kendaaaraan --</option>
                        <?=GetCombobox("SELECT * FROM mvehicletype ORDER BY NM_Type", COL_ID_TYPE, COL_NM_TYPE, ($edit ? $data[COL_ID_TYPE] : null))?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Nama Pemilik (Sesuai STNK)</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="<?=COL_NM_PEMILIK?>" value="<?= $edit ? $data[COL_NM_PEMILIK] : ""?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">No. Plat</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="<?=COL_NO_PLAT?>" value="<?= $edit ? $data[COL_NO_PLAT] : ""?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Tahun Pembuatan</label>
                <div class="col-sm-3">
                    <input type="number" class="form-control" name="<?=COL_TH_PEMBUATAN?>" value="<?= $edit ? $data[COL_TH_PEMBUATAN] : ""?>" required>
                </div>
            </div>
        </div>
        <div class="col-md-12 pt-3" style="border-top: 1px solid #dedede; text-align: right">
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> SUBMIT</button>
        </div>
    </div>
<?=form_close()?>