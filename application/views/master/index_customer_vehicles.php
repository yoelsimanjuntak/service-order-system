<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 29/01/2020
 * Time: 00:52
 */
$ruser = GetLoggedUser();
?>
<?php
if($ruser[COL_ROLEID] == ROLEADMIN) {
    ?>
    <p>
        <a href="<?=site_url('master/customer-vehicle-delete')?>" class="btn btn-xs btn-danger" id="btn-del-vehicle" data-confirm="Yakin ingin menghapus?"><i class="fas fa-trash"></i> HAPUS</a>
        <button type="button" class="btn btn-xs btn-primary" id="btn-add-vehicle"><i class="fas fa-pencil-square-o"></i> TAMBAH</button>
    </p>
<?php
}
?>
<form id="dataform" method="post" action="#">
    <table class="table table-bordered" style="font-size: 11pt">
        <thead>
        <tr>
            <?php
            if($ruser[COL_ROLEID] == ROLEADMIN) {
                ?>
                <th style="width: 10px">
                    <input type="checkbox" id="cekbox" />
                </th>
            <?php
            }
            ?>
            <th>Tipe</th>
            <th>Nama Pemilik</th>
            <th>No. Plat</th>
            <th>Tahun Pembuatan</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $vehicles = $this->db
            ->select("mvehicle.*, mvehicletype.NM_Type")
            ->join(TBL_MVEHICLETYPE,TBL_MVEHICLETYPE.'.'.COL_ID_TYPE." = ".TBL_MVEHICLE.".".COL_ID_TYPE,"left")
            ->where(COL_ID_CUSTOMER, $data[COL_ID_CUSTOMER])
            ->order_by(TBL_MVEHICLETYPE.".".COL_NM_TYPE)
            ->get(TBL_MVEHICLE)
            ->result_array();
        foreach($vehicles as $t) {
            ?>
            <tr>
                <?php
                if($ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <td><input type="checkbox" class="cekbox" name="cekbox[]" value="<?=$t[COL_ID_VEHICLE]?>" /></td>
                <?php
                }
                ?>
                <td>
                    <?php
                    if($ruser[COL_ROLEID] == ROLEADMIN) {
                        ?>
                        <a href="javascript: void(0)" onclick="getDetailVehicle('<?=site_url("master/customer-vehicle-edit/".$t[COL_ID_VEHICLE]."/1")?>')"><?=$t[COL_NM_TYPE]?></a>
                    <?php
                    }
                    ?>
                </td>
                <td><?=$t[COL_NM_PEMILIK]?></td>
                <td><?=$t[COL_NO_PLAT]?></td>
                <td><?=$t[COL_TH_PEMBUATAN]?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</form>
<div class="modal fade" id="modal-vehicle">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Kendaran</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Loading ...</p>
            </div>
        </div>
    </div>
</div>
<script>
    function refreshTask() {
        $("[data-card-widget=card-refresh]", $("#card-vehicles")).click();
    }
    function getDetailVehicle(url) {
        var modal = $("#modal-vehicle");
        modal.modal('show');

        $(".modal-body", modal).load(url, function() {
            $("select", modal).not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
            $('.datepicker', modal).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1970,
                maxYear: parseInt(moment().format('YYYY'),10),
                locale: {
                    format: 'Y-MM-DD'
                }
            });
            $("#vehicle-form", modal).validate({
                submitHandler : function(form){
                    modal.find(".modal-body").append('<div class="overlay"><i class="fas fa-2x fa-sync-alt"></i></div>');
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        type : 'post',
                        success : function(data){
                            if(data.error != 0){
                                toastr.error(data.error);
                            }else{
                                toastr.success('Berhasil memperbarui data kendaraan.');
                                modal.modal("hide");
                            }
                        },
                        error : function(a,b,c){
                            toastr.error(a.responseText);
                            modal.modal("hide");
                        },
                        complete : function() {
                            modal.find(".overlay").remove();
                            refreshTask();
                        }
                    });
                    return false;
                }
            });
        });
    }
    $(document).ready(function() {
        $("#btn-add-vehicle").click(function() {
            var modal = $("#modal-vehicle");
            modal.modal('show');

            $(".modal-body", modal).load('<?=site_url("master/customer-vehicle-add/".$data[COL_ID_CUSTOMER]."/1")?>', function() {
                $("select", modal).not('.no-select2').select2({ width: 'resolve', theme: 'bootstrap4' });
                $('.datepicker', modal).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1970,
                    maxYear: parseInt(moment().format('YYYY'),10),
                    locale: {
                        format: 'Y-MM-DD'
                    }
                });
                $("#vehicle-form", modal).validate({
                    submitHandler : function(form){
                        modal.find(".modal-body").append('<div class="overlay"><i class="fas fa-2x fa-sync-alt"></i></div>');
                        $(form).ajaxSubmit({
                            dataType: 'json',
                            type : 'post',
                            success : function(data){
                                if(data.error != 0){
                                    toastr.error(data.error);
                                }else{
                                    toastr.success('Berhasil menambah data kendaraan.');
                                    modal.modal("hide");
                                }
                            },
                            error : function(a,b,c){
                                toastr.error(a.responseText);
                                modal.modal("hide");
                            },
                            complete : function() {
                                modal.find(".overlay").remove();
                                refreshTask();
                            }
                        });
                        return false;
                    }
                });
            });
        });
        $('#btn-del-vehicle').click(function(){
            var a = $(this);
            var confirmDialog = $("#confirmDialog");
            var alertDialog = $("#alertDialog");

            confirmDialog.on("hidden.bs.modal", function(){
                $(".modal-body", confirmDialog).html("");
            });
            alertDialog.on("hidden.bs.modal", function(){
                $(".modal-body", alertDialog).html("");
            });

            if($('.cekbox:checked').length < 1){
                $(".modal-body", alertDialog).html("No data selected.");
                alertDialog.modal("show");
                //alert('Tidak ada data dipilih');
                return false;
            }

            $(".modal-body", confirmDialog).html("Yakin ingin menghapus?");
            confirmDialog.modal("show");
            $(".btn-ok", confirmDialog).click(function() {
                $(this).html("Loading...").attr("disabled", true);
                $('#dataform').ajaxSubmit({
                    dataType: 'json',
                    url : a.attr('href'),
                    success : function(data){
                        if(data.error==0){
                            //alert(data.success);
                            window.location.reload();
                        }else{
                            //alert(data.error);
                            $(".modal-body", alertDialog).html(data.error);
                            alertDialog.modal("show");
                        }
                    },
                    complete: function(){
                        $(this).html("OK").attr("disabled", false);
                        confirmDialog.modal("hide");
                    }
                });
            });
            return false;
        });
        $('#cekbox').click(function(){
            if($(this).is(':checked')){
                $('.cekbox').prop('checked',true);
            }else{
                $('.cekbox').prop('checked',false);
            }
        });
    });
</script>