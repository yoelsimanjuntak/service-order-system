<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/1/2019
 * Time: 10:51 PM
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=site_url('master/customer-index')?>"> <?=$title?></a></li>
                        <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-primary">
                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'userForm','class'=>'form-horizontal'))?>
                        <div class="card-body">
                            <div style="display: none" class="alert alert-danger errorBox">
                                <i class="fa fa-ban"></i> Error :
                                <span class="errorMsg"></span>
                            </div>
                            <?php
                            if($this->input->get('error') == 1){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <span class="">Data gagal disimpan, silahkan coba kembali</span>
                                </div>
                            <?php
                            }
                            if(validation_errors()){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=validation_errors()?>
                                </div>
                            <?php
                            }
                            if(!empty($upload_errors)) {
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=$upload_errors?>
                                </div>
                            <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">Nama Konsumen</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="<?=COL_NM_CUSTOMER?>" value="<?= $edit ? $data[COL_NM_CUSTOMER] : ""?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">Alamat</label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" rows="5" name="<?=COL_NM_ADDRESS?>" required="true"><?= $edit ? $data[COL_NM_ADDRESS] : ""?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">No. Telp / HP / WA</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="<?=COL_NM_PHONENO?>" value="<?= $edit ? $data[COL_NM_PHONENO] : ""?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">Email</label>
                                        <div class="col-sm-3">
                                            <input type="email" class="form-control" name="<?=COL_NM_EMAIL?>" value="<?= $edit ? $data[COL_NM_EMAIL] : ""?>" required>
                                        </div>
                                    </div>
                                    <?php
                                    if(empty($data)) {
                                        ?>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-3">Tipe Kendaraan</label>
                                            <div class="col-sm-5">
                                                <select name="<?=COL_ID_TYPE?>" class="form-control" required>
                                                    <option value="">-- Pilih Tipe Kendaaaraan --</option>
                                                    <?=GetCombobox("SELECT * FROM mvehicletype ORDER BY NM_Type", COL_ID_TYPE, COL_NM_TYPE, null)?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-3">Nama Pemilik (Sesuai STNK)</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="<?=COL_NM_PEMILIK?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-3">No. Plat</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="<?=COL_NO_PLAT?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-3">Tahun Pembuatan</label>
                                            <div class="col-sm-3">
                                                <input type="number" class="form-control" name="<?=COL_TH_PEMBUATAN?>" required>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?=site_url('master/customer-index')?>" class="btn btn-default">BACK</a>
                                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                    <?php
                    if(!empty($data)) {
                        ?>
                        <div id="card-vehicles" class="card card-outline card-success">
                            <div class="card-header">
                                <h3 class="card-title">Kendaraan</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?=site_url('master/customer-vehicles/'.$data[COL_ID_CUSTOMER])?>"><i class="fas fa-sync-alt"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">

                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $("[data-card-widget=card-refresh]", $("#card-vehicles")).click();
            $("#modal-vehicle").on('hidden.bs.modal', function(){
                $(this).find(".modal-body").html("Loading...");
            });
        });
    </script>
<?php $this->load->view('footer') ?>