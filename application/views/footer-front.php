</div>
<div id="footer-section" style="position: relative; z-index:9; border-top: 1px solid #dee2e6;">
  <div class="content" style="padding: 0 .5rem">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4 p-2">
          <div class="row">
            <div class="col-lg-4">
              <p class="text-center">
                <img class="mb-3" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" height="80px" /><br  />
                <img class="mb-3" src="<?=MY_IMAGEURL.'company-logo.png'?>" height="80px" /><br  />
                <img class="mb-3" src="<?=MY_IMAGEURL.'yamaha-logo.png'?>" height="80px" />
              </p>
            </div>
            <div class="col-lg-8">
              <p>
                <b><?=$this->setting_org_name?></b><br  />
                <?=$this->setting_org_address?>
              </p>
              <p>
                Telp: <?=$this->setting_org_phone?>, Fax: <?=$this->setting_org_fax?><br  />
                Email: <?=$this->setting_org_mail?>
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 p-2">
          <form action="#">
            <h6>HUBUNGI KAMI</h6>
            <div class="col-lg-12">
              <div class="form-group row">
                <div class="col-lg-12">
                  <input type="text" class="form-control" placeholder="Nama Lengkap" />
                </div>
              </div>
              <div class="form-group row">
                <div class="col-lg-6">
                  <input type="text" class="form-control" placeholder="No. Telp" />
                </div>
                <div class="col-lg-6">
                  <input type="email" class="form-control" placeholder="Email" />
                </div>
              </div>
              <div class="form-group row">
                <div class="col-lg-12">
                  <textarea class="form-control" rows="3" placeholder="Pesan"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-lg-12 text-right">
                  <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> SUBMIT</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-4 p-2">
          <h6>LINK</h6>
          <p>
            <a class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook fa-2x"></i></a>
            <a class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram fa-2x"></i></a>
            <a class="btn btn-social-icon btn-instagram"><i class="fa fa-twitter fa-2x"></i></a>
          </p>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<footer class="main-footer">
    <div class="float-right d-none d-sm-inline">
        <b>Version</b> <?=$this->setting_web_version?>
    </div>
    <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>.
</footer>
</div>
</body>
</html>
