<?php
$this->load->view('header');
$ruser = GetLoggedUser();
?>
<style>
    .info-box-icon {
        height: 70px !important;
        line-height: 70px !important;
    }
    .info-box-icon-right {
        border-top-right-radius: .25rem;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: .25rem;
        display: block;
        float: right;
        height: 70px !important;
        width: 24px;
        text-align: center;
        font-size: 12px;
        line-height: 70px;
        #background: rgba(0,0,0,0.1);
    }
    .info-box-icon-right:hover {
        background: rgba(0,0,0,0.15);
    }
    .info-box-icon-right>.small-box-footer {
        color: rgba(255,255,255,0.8);
    }
    .info-box-icon-right>.small-box-footer:hover {
        color: #fff;
    }
    .info-box .info-box-content {
        padding: 3px 10px !important;
    }
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">

        </div>
    </div>
</section>
<?php $this->load->view('loadjs') ?>
<?php $this->load->view('footer') ?>

