<?php $this->load->view('header-front') ?>
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Beranda</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="card-service" class="card">
                    <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'order-form','class'=>'form-horizontal'))?>
                    <div class="card-header">
                        <h5 class="card-title m-0">Silakan isi form untuk mendaftar.</h5>
                    </div>
                    <div class="card-body">
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i> Error :
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('error') == 1){
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <span class="">Data gagal disimpan, silahkan coba kembali.</span>
                            </div>
                        <?php
                        }
                        if(validation_errors()){
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?=validation_errors()?>
                            </div>
                        <?php
                        }
                        if(!empty($upload_errors)) {
                            ?>
                            <div class="alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                <?=$upload_errors?>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">Nama Konsumen</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="<?=COL_NM_CUSTOMER?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">Alamat</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" rows="5" name="<?=COL_NM_ADDRESS?>" required="true"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">No. Telp / HP / WA</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="<?=COL_NM_PHONENO?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">Email</label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control" name="<?=COL_NM_EMAIL?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">Password</label>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control" name="<?=COL_PASSWORD?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">Konfirmasi Password</label>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control" name="RepeatPassword" required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 pl-2">
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">Tipe Kendaraan</label>
                                    <div class="col-sm-8">
                                        <select name="<?=COL_ID_TYPE?>" class="form-control" required>
                                            <option value="">-- Pilih Tipe Kendaraan --</option>
                                            <?=GetCombobox("SELECT * FROM mvehicletype ORDER BY NM_Type", COL_ID_TYPE, COL_NM_TYPE, null)?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">Nama Pemilik</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="<?=COL_NM_PEMILIK?>" placeholder="Sesuai STNK" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">No. Plat</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="<?=COL_NO_PLAT?>" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label col-sm-4">Tahun Pembuatan</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" name="<?=COL_TH_PEMBUATAN?>" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                    <?=form_close()?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('loadjs') ?>
<?php $this->load->view('footer-front') ?>